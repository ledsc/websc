import logging

logger = logging.getLogger("websc")


class LedSCLogFormatter(logging.Formatter):
    blue = "\x1b[34;20m"
    green = "\u001b[32m"
    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = f"%(asctime)s [%(levelname)-4.4s] - %(message)s {reset}"
    # format = f"%(asctime)s [%(levelname)-4.4s] - %(message)s {grey}(%(filename)s:%(lineno)d){reset}"
    # "%(asctime)s [%(levelname)-4.4s]  %(message)s"

    FORMATS = {
        logging.DEBUG: blue + format + reset,
        logging.INFO: green + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        datefmt = '%H:%M:%S'
        formatter = logging.Formatter(log_fmt, datefmt=datefmt)
        return formatter.format(record)
