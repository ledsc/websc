import time
from enum import Enum
from typing import Dict, List, Callable

from ledsc import LedSC
from .events import EventQueue, EventUpdate
from .log import logger


class LedSCProxy:
    # NOTE: poradi musi korespondovat s poradi registru
    regs_ctrl = ['R', 'G', 'B', 'W', 'TT', 'LM', 'PWMC', 'PX_CNT', 'is_lost']
    regs_conf = ['MB_ID', 'SER_BD', 'SER_P',
                 'LCH_R', 'LCH_G', 'LCH_B', 'LCH_W',
                 'DF_TT', 'DF_R', 'DF_G', 'DF_B', 'DF_W',
                 'GR_EN', 'PX_EN', 'DF_PWMC', 'LOS', 'is_lost'
                 ]

    def __init__(self, device_name: str, device_driver: LedSC, event_queue_ctrl: EventQueue,
                 event_queue_conf: EventQueue, refresh_delay: float = 0.2):
        self.name = device_name
        self.device_driver = device_driver
        self.event_queue_ctrl = event_queue_ctrl
        self.event_queue_conf = event_queue_conf
        self.is_lost: bool = False
        self.__write_timestamp = 0
        self.__refresh_delay = refresh_delay

        self.data_ctrl: Dict[str, int] = {
            i: 0 for i in self.regs_ctrl
        }

        self.data_conf: Dict[str, int] = {
            i: 0 for i in self.regs_conf
        }

    def force_update(self, scan_config: bool):
        raise NotImplemented("This method must be replaced after assignment from the bus!")

    def refresh_ctrl(self):
        # TODO: delay only if more action in buffer (reference to event manager?)
        if time.time() - self.__write_timestamp > self.__refresh_delay:
            new_data = self.device_driver.read_register(address=0, count=8)
            new_data.append(self.is_lost)

            if time.time() - self.__write_timestamp > self.__refresh_delay:
                changed = self.__refresh(regs=self.regs_ctrl, data=self.data_ctrl, new_data=new_data)
                if len(changed) > 0:
                    self.event_queue_ctrl.add_event(EventUpdate({'dev': {self.name: changed}}))

    def refresh_conf(self):
        def_rgbw = self.device_driver.get_default_rgbw()
        led_chs = self.device_driver.get_channel_configuration()
        new_data = [
            self.device_driver.get_mb_slave_id(),
            self.device_driver.get_serial_baudrate(),
            self.device_driver.get_serial_parity(),
            led_chs[0],
            led_chs[1],
            led_chs[2],
            led_chs[3],
            self.device_driver.get_default_transition_time(),
            def_rgbw[0],
            def_rgbw[1],
            def_rgbw[2],
            def_rgbw[3],
            self.device_driver.get_greeting_enable(),
            self.device_driver.get_px_enable(),
            self.device_driver.get_default_pwm_cycle(),
            self.device_driver.get_light_on_start(),
            self.is_lost
        ]
        changed = self.__refresh(regs=self.regs_conf, data=self.data_conf, new_data=new_data)
        if len(changed) > 0:
            self.event_queue_conf.add_event(EventUpdate({'dev': {self.name: changed}}))

    @staticmethod
    def __refresh(regs: List[str], data: Dict[str, int], new_data: List[int]) -> Dict[str, int]:
        changed = {}
        for i in range(len(regs)):
            key = regs[i]
            if new_data[i] != data[key]:
                data[key] = new_data[i]
                changed[key] = new_data[i]
        return changed

    def set_data_ctrl(self, data_ctrl: dict):
        for key, value in data_ctrl.items():
            if value is None:
                continue
            if key == 'R':
                self.device_driver.set_red(value)
            elif key == 'G':
                self.device_driver.set_green(value)
            elif key == 'B':
                self.device_driver.set_blue(value)
            elif key == 'W':
                self.device_driver.set_white(value)
            elif key == 'TT':
                self.device_driver.set_transition_time(value)
            elif key == 'LM':
                self.device_driver.set_live_mode(value)
            elif key == 'PWMC':
                self.device_driver.set_pwm_cycle(value)
            elif key == 'PX_CNT':
                self.device_driver.set_px_counter(value)
            elif key == 'trigger':
                if int(value) != 0:
                    self.device_driver.do_px_trigger()
            else:
                logger.error(f"LedSCProxy: set_data_ctrl: Unsupported key: '{key}'")
            self.__write_timestamp = time.time()

    def set_data_conf(self, data_conf: dict):
        channel_configuration = self.device_driver.get_channel_configuration()
        channel_configuration_changed = False
        default_color = self.device_driver.get_default_rgbw()
        default_color_changed = False
        for key, value in data_conf.items():
            if value is None:
                continue
            elif key == 'MB_ID':
                self.device_driver.set_mb_slave_id(value)
            elif key == 'SER_BD':
                self.device_driver.set_serial_baudrate(value)
            elif key == 'SER_P':
                self.device_driver.set_serial_parity(value)
            elif key == 'LCH_R':
                channel_configuration_changed = True
                channel_configuration[0] = int(value)
            elif key == 'LCH_G':
                channel_configuration_changed = True
                channel_configuration[1] = int(value)
            elif key == 'LCH_B':
                channel_configuration_changed = True
                channel_configuration[2] = int(value)
            elif key == 'LCH_W':
                channel_configuration_changed = True
                channel_configuration[3] = int(value)
            elif key == 'DF_TT':
                self.device_driver.set_default_transition_time(value)
            elif key == 'DF_R':
                default_color_changed = True
                default_color[0] = int(value)
            elif key == 'DF_G':
                default_color_changed = True
                default_color[1] = int(value)
            elif key == 'DF_B':
                default_color_changed = True
                default_color[2] = int(value)
            elif key == 'DF_W':
                default_color_changed = True
                default_color[3] = int(value)
            elif key == 'GR_EN':
                self.device_driver.set_greeting_enable(value)
            elif key == 'PX_EN':
                self.device_driver.set_px_enable(value)
            elif key == 'DF_PWMC':
                self.device_driver.set_default_pwm_cycle(value)
            elif key == 'LOS':
                self.device_driver.set_light_on_start(value)
            elif key == 'bus':
                pass
            else:
                logger.error(f"LedSCProxy: set_data_conf: Unsupported key: '{key}'")
        if channel_configuration_changed:
            self.device_driver.set_channel_configuration(*channel_configuration)
        if default_color_changed:
            self.device_driver.set_default_rgbw(*default_color)
        self.force_update(True)

    def do_reset(self):
        logger.info(f"Restarting LedSC: '{self.name}'")
