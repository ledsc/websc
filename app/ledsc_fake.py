from typing import Dict, List

from ledsc import LedSC
from pymodbus.client import ModbusSerialClient
from pymodbus.pdu import ModbusResponse


class LedSCFake(LedSC):
    def __init__(self, modbus_client: ModbusSerialClient, slave_id: int):
        super().__init__(modbus_client=modbus_client, slave_id=slave_id)
        self.registers: Dict[int, int] = {
            0: 0,
            1: 0,
            2: 0,
            3: 0,
            4: 100,
            5: 1,
            6: 255,
            7: 0,
            8: 0,
            100: 0,
            1000: self.slave_id,
            1001: self.client.comm_params.baudrate,
            1002: self.client.comm_params.parity,
            1003: 0,
            1004: 1,
            1005: 2,
            1006: 3,
            1007: 100,
            1008: 0,
            1009: 0,
            1010: 0,
            1011: 100,
            1012: 1,
            1013: 1,
            1014: 255,
            1015: 0,
        }
        self.on_state = [self.registers[1008], self.registers[1009], self.registers[1010], self.registers[1011]]

    @staticmethod
    def __get_mb_addresses(address: int, count: int) -> List[int]:
        address = list(range(address, address + count))
        return address

    def read_register(self, address: int, count: int = 1):
        ret = list()
        for addr in self.__get_mb_addresses(address, count=count):
            if addr not in self.registers:
                raise ValueError(f"Address {address} is not in registers")
            ret.append(self.registers[addr])
        ret = ret if len(ret) > 1 else ret[0]
        return ret

    def write_register(self, address: int, value: int):
        if address not in self.registers:
            raise ValueError(f"Address {address} is not in registers")
        if address == 100:
            print("Resetting device")
            return
        elif address in range(0, 4) and int(value) > 0:
            for i in range(0, 4):
                self.on_state[i] = self.registers[i]
        elif address == 8:
            if self.registers[0] == 0 and self.registers[1] == 0 and self.registers[2] == 0 and self.registers[3] == 0:
                for i in range(0, 4):
                    self.registers[i] = self.on_state[i]
            else:
                for i in range(0, 4):
                    self.registers[i] = 0
            self.registers[7] += 1
            return
        self.registers[address] = value

    def write_registers(self, address: int, values: List[int]):
        for addr in self.__get_mb_addresses(address, count=len(values)):
            self.write_register(addr, values[addr-address])
