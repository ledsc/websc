import logging
import time
import traceback
from threading import Semaphore, Thread, Condition
from typing import Type, Any, Callable, List

from pymodbus.client import ModbusSerialClient
from pymodbus.exceptions import ModbusIOException
from pymodbus.framer import ModbusFramer, ModbusRtuFramer

from .ledsc_proxy import LedSCProxy
from .log import logger


class ThreadingModbusSerialClient(ModbusSerialClient):
    def __init__(self, port: str, framer: Type[ModbusFramer] = ModbusRtuFramer, baudrate: int = 19200,
                 bytesize: int = 8, parity: str = "N", stopbits: int = 1, **kwargs: Any) -> None:
        super().__init__(port, framer, baudrate, bytesize, parity, stopbits, **kwargs)
        # TODO: kontrola, jestli cesta existuje!
        for method_name in ['write_register', 'write_registers', 'read_holding_registers']:
            setattr(self, method_name, self.__block(getattr(self, method_name)))
        self.lock = Semaphore(1)

    def __block(self, operation: Callable):
        def ret(*args, **kwargs):
            self.lock.acquire()
            # logger.debug(f"LedSCBus: calling operation: {operation.__name__}({args}, {kwargs})")
            aret = operation(*args, **kwargs)
            self.lock.release()
            return aret
        return ret


class BusTask(Thread):
    def __init__(self, bus_path: str, devices: List[LedSCProxy], frequency: float = 1):
        super().__init__()
        self.bus_path = bus_path
        self.devices = devices
        self.killed = False
        self.period = 1 / frequency
        self.scan_config = True
        self.condition = Semaphore(0)
        for device in self.devices:
            device.force_update = self.force_update

    def force_update(self, scan_config: bool):
        self.scan_config = scan_config
        self.condition.release()

    def run(self):
        logger.info(f"Starting BusTask for '{self.bus_path}'")
        while not self.killed:
            try:
                for device in self.devices:
                    try:
                        device.refresh_ctrl()
                        if self.scan_config:
                            device.refresh_conf()
                        device.is_lost = False
                    except ModbusIOException as E:
                        # TODO: ztracene zarizeni skenovat mene casto
                        if not device.is_lost:
                            device.is_lost = True
                            logger.error(f"Error while refreshing device '{device.name}': {E}")
                            if logger.level == logging.DEBUG:
                                traceback.print_exc()
                if self.scan_config:
                    self.scan_config = False
                self.condition.acquire(timeout=self.period)
            except Exception as E:
                logger.error(f"Error in BusTask: {E}")
                if logger.level == logging.DEBUG:
                    traceback.print_exc()
        logger.info(f"Stopping BusTask for '{self.bus_path}'")
