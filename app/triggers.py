import time
from copy import copy
from typing import List, Dict, Union, Callable

from . import LedSCProxy, EventQueue
from .configuration import ConfigurationTrigger, TriggerEvent, ConfigurationConsequence
from .events import EventRequest
from .log import logger

regs_ctrl = copy(LedSCProxy.regs_ctrl)
regs_ctrl.extend(['color'])


class TriggerMaster:
    CYCLING_SECURITY_TIME = 0.1

    def __init__(self, devices: Dict[str, LedSCProxy], event_queue: EventQueue):
        self.running = False
        self.__triggers: Dict[str, Dict[TriggerEvent, List[ConfigurationConsequence]]] = {}
        self.devices = devices
        self.event_queue = event_queue
        self._trigered: Dict[str, tuple[str, float]] = {}

    def set_devices(self, devices: Dict[str, LedSCProxy]):
        self.devices = devices

    @property
    def triggers(self):
        raise NotImplemented(f"TriggerMaster: Triggers not existing here in raw structure.")

    @triggers.setter
    def triggers(self, value: List[ConfigurationTrigger]):
        self.__triggers = {}
        logger.info(f"TriggerMaster: Loading triggers metadata.")
        for trigger in value:
            if trigger.event != TriggerEvent.NONE:
                if trigger.device not in self.__triggers:
                    self.__triggers[trigger.device] = {}
                self.__triggers[trigger.device][trigger.event] = trigger.consequences

    def start(self, triggers: List[ConfigurationTrigger]):
        self.triggers = triggers
        self.running = True

    def stop(self):
        self.running = False

    @staticmethod
    def __get_event(data: dict) -> List[TriggerEvent]:
        """
        Get trigger event from data
        Order in switch determines the priority of the event
        :return:
        """
        ret = list()
        data = data.keys()
        if "R" in data or "G" in data or "B" in data or "W" in data:
            ret.append(TriggerEvent.COLOR)
        if "PX_CNT" in data:
            ret.append(TriggerEvent.PX)
        return ret

    def exec_consequences(self, consequences: List[ConfigurationConsequence]):
        for consequence in consequences:
            if str(consequence.set_dev) not in self.devices:
                logger.error(f"TriggerMaster: Device '{consequence.set_dev}' not found.")
                continue
            elif consequence.set_var != "trigger" and consequence.set_var not in regs_ctrl:
                logger.error(f"TriggerMaster: Variable '{consequence.set_var}' not found.")
                continue
            elif consequence.set_var == 'color' and consequence.get_var != 'color':
                logger.error(
                    f"TriggerMaster: Unsupported variables combination: '{consequence.set_var}':{consequence.get_var}'.")
                continue
            elif consequence.get_dev != "<value>" and str(consequence.get_dev) not in self.devices:
                logger.error(f"TriggerMaster: Device '{consequence.get_dev}' not found.")
                continue
            elif consequence.get_dev != "<value>" and consequence.get_var not in regs_ctrl:
                logger.error(f"TriggerMaster: Variable '{consequence.get_var}' not found.")
                continue
            elif self.cycling_security(consequence):
                continue

            if consequence.set_var == "color":
                req = {
                    "R": self.devices[consequence.get_dev].data_ctrl['R'],
                    "G": self.devices[consequence.get_dev].data_ctrl['G'],
                    "B": self.devices[consequence.get_dev].data_ctrl['B'],
                    "W": self.devices[consequence.get_dev].data_ctrl['W'],
                }
            elif consequence.get_dev == "<value>":
                req = {
                    consequence.set_var: int(consequence.get_var),
                }
            else:
                req = {
                    consequence.set_var: self.devices[consequence.get_dev].data_ctrl[consequence.get_var]
                }

            if consequence.set_dev in self.__triggers:
                self._trigered[consequence.set_dev] = (consequence.get_dev, time.time())
            self.event_queue.add_event(EventRequest({'dev': {consequence.set_dev: req}}))

    def cycling_security(self, consequence: ConfigurationConsequence) -> bool:
        if consequence.get_dev in self._trigered:
            set_dev, timestamp = self._trigered.pop(consequence.get_dev)
            if time.time() - timestamp <= self.CYCLING_SECURITY_TIME and consequence.set_dev == set_dev:
                # logger.debug(f"Cycling security for '{consequence.get_dev}' triggered by '{set_dev}'")
                return True
        return False

    def do_trigger(self, trigger: ConfigurationTrigger):
        self.exec_consequences(trigger.consequences)

    def do_check(self, request: dict):
        if not self.running or len(self.__triggers) == 0 or 'dev' not in request:
            return
        for name, data in request['dev'].items():
            if name in self.__triggers:
                for event in self.__get_event(data):
                    if event in self.__triggers[name]:
                        self.exec_consequences(self.__triggers[name][event])
