import _queue
import threading
from queue import Queue
from typing import List, Callable, Dict, Type

from .log import logger


class Event:
    def __init__(self, data: dict):
        self.data = data


class EventUpdate(Event):
    pass


class EventRequest(Event):
    pass


class EventQueue:
    def __init__(self, enabled: bool = True):
        self.enabled = enabled
        self.queue = Queue()

    def add_event(self, event: Event):
        if self.enabled:
            self.queue.put(event)

    def get_events(self, timeout: float = None) -> List[Event]:
        ret = [self.queue.get(timeout=timeout)]
        while not self.queue.empty():
            ret.append(self.queue.get())
        return ret


class EventExecutor(threading.Thread):
    def __init__(self, event_queue: EventQueue):
        super().__init__()
        self.event_queue = event_queue
        self.handlers: Dict[Type[Event], List[Callable]] = {}
        self.killed = False

    def register_handler(self, event: Type[Event], handler: Callable):
        if event not in self.handlers:
            self.handlers[event] = []
        self.handlers[event].append(handler)

    def run(self):
        while not self.killed:
            try:
                last_event = None
                data = {}
                for event in self.event_queue.get_events(timeout=0.5):
                    if type(event) not in self.handlers:
                        logger.warning(f"handler for event type '{type(event)}' not registered!")
                        continue
                    data.update(event.data)
                    if last_event is not None and last_event != type(event):
                        for handler in self.handlers[last_event]:
                            handler(data)
                    last_event = type(event)
                for handler in self.handlers[last_event]:
                    handler(data)
            except _queue.Empty:
                pass

