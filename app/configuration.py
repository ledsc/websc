import logging
from collections import OrderedDict
from enum import Enum
from typing import List, Union
import yaml

from .log import logger

# Add ordered dictionary support
yaml.add_representer(OrderedDict, lambda dumper, data: dumper.represent_mapping('tag:yaml.org,2002:map', data.items()))


class TriggerEvent(Enum):
    NONE = "none"
    PX = "px"
    COLOR = "color"


class ConfigurationSlave:
    def __init__(self, name: str, slave_id: int, bus: str = None):
        self.name = name
        self.slave_id = slave_id
        self.bus: Union[None, str] = bus


class ConfigurationBus:
    def __init__(self, name: str, path: str, baud_rate: int, slaves: List[ConfigurationSlave], parity: str = 'N'):
        self.name = name
        self.path = path
        self.baud_rate = baud_rate
        self.parity = parity
        self.slaves: List[ConfigurationSlave] = slaves
        for slave in slaves:
            slave.bus = self.name


class ConfigurationServer:
    def __init__(self, enabled: bool, host: str, port: int):
        self.enabled = enabled
        self.host = host
        self.port = port


class ConfigurationConsequence:
    def __init__(self, data: dict):
        _set = data["set"]
        _get = data["get"]
        self.set_dev = _set["dev"]
        self.set_var = _set["var"]
        self.get_dev = _get["dev"]
        self.get_var = _get["var"]

    @property
    def dict(self):
        return {
            "set": {"dev": self.set_dev, "var": self.set_var},
            "get": {"dev": self.get_dev, "var": self.get_var},
        }


class ConfigurationTrigger:
    def __init__(self, name: str, event: TriggerEvent, device: Union[None, str],
                 consequences: List[ConfigurationConsequence]):
        self.name = name
        self.event = event
        self.device = device
        self.consequences = consequences

    @property
    def dict(self):
        return {
            'event': self.event.value,
            'device': self.device,
            'consequences': [c.dict for c in self.consequences],
        }


class ConfigurationStructure:
    def __init__(self, path: Union[None, str], log_level: int, buses: List[ConfigurationBus], virtual: bool,
                 server_web: Union[None, ConfigurationServer],
                 server_ctrl: Union[None, ConfigurationServer],
                 server_conf: Union[None, ConfigurationServer],
                 triggers: List[ConfigurationTrigger]):
        """
        :param path: The path to the configuration file
        :param log_level: logging level
        :param buses: list of communication buses
        :param server_web: configuration for web (None is disabled)
        :param server_ctrl: configuration for ctrl (None is disabled)
        :param server_conf: configuration for configuration ctrl (None is disabled)
        """
        self.path = path
        self.server_web = server_web
        self.server_ctrl = server_ctrl
        self.server_conf = server_conf
        self.log_level = log_level
        self.buses = buses
        self.triggers = triggers
        self.virtual = virtual


class ConfigurationParser:
    @staticmethod
    def __parse_server(data: Union[None, dict]) -> Union[None, ConfigurationServer]:
        if data is None:
            return None
        return ConfigurationServer(
            enabled=data.get("enabled", True),
            host=data.get('host', '0.0.0.0'),
            port=data.get('port')
        )

    @staticmethod
    def __parse_slave(name: str, data: dict) -> ConfigurationSlave:
        return ConfigurationSlave(
            name=name,
            slave_id=data.get("slave-id")
        )

    @staticmethod
    def __parse_bus(name: str, data: dict) -> ConfigurationBus:
        self = ConfigurationParser
        return ConfigurationBus(
            name=name,
            path=data.get('path'),
            baud_rate=data.get('baud-rate', 19200),
            parity=data.get('parity', 'N'),
            slaves=[self.__parse_slave(n, s) for n, s in data.get("slaves", {}).items()]
        )

    @staticmethod
    def __parse_trigger(name: str, data: dict) -> ConfigurationTrigger:
        return ConfigurationTrigger(
            name=name,
            event=TriggerEvent(data.get("event")),
            device=data.get("device", None),
            consequences=[ConfigurationConsequence(d) for d in data.get("consequences", [])]
        )

    @staticmethod
    def load(path: str) -> ConfigurationStructure:
        self = ConfigurationParser
        data = yaml.load(open(path, 'r'), Loader=yaml.Loader)
        return ConfigurationStructure(
            path=path,
            server_web=self.__parse_server(data.get("server-web", None)),
            server_ctrl=self.__parse_server(data.get("server-ctrl", None)),
            server_conf=self.__parse_server(data.get("server-conf", None)),
            log_level=data.get('log-level', logging.WARNING),
            buses=[self.__parse_bus(n, s) for n, s in data.get("buses", {}).items()],
            triggers=[self.__parse_trigger(n, e) for n, e in data.get("triggers", {}).items()],
            virtual=data.get('virtual', False)
        )

    @staticmethod
    def __encode_server(data: ConfigurationServer) -> dict:
        return {'host': data.host, 'port': data.port}

    @staticmethod
    def __encode_slave(data: ConfigurationSlave) -> dict:
        return {'slave-id': data.slave_id}

    @staticmethod
    def __encode_bus(data: ConfigurationBus) -> dict:
        self = ConfigurationParser
        return {
            'path': data.path, 'baud-rate': data.baud_rate, 'parity': data.parity,
            'slaves': {s.name: self.__encode_slave(s) for s in data.slaves}
        }

    @staticmethod
    def __encode_trigger(data: ConfigurationTrigger) -> dict:
        ret = OrderedDict({'event': data.event.value})
        if data.event is not TriggerEvent.NONE:
            ret['device'] = data.device
        if data.consequences is not None and len(data.consequences):
            ret['consequences'] = [
                {
                    'set': {'dev': c.set_dev, 'var': c.set_var},
                    'get': {'dev': c.get_dev, 'var': int(c.get_var) if c.get_dev == "<value>" else c.get_var}
                }
                for c in data.consequences
            ]
        return ret

    @staticmethod
    def save(configuration: ConfigurationStructure):
        self = ConfigurationParser
        if configuration.path is None:
            logger.error(f"Virtual configuration cannot be saved! Please load configuration from file.")
            return

        data = OrderedDict({'log-level': configuration.log_level})
        if configuration.virtual:
            data['virtual'] = configuration.virtual
        for key, server in [('server-web', configuration.server_web),
                            ('server-ctrl', configuration.server_ctrl),
                            ('server-conf', configuration.server_conf)]:
            if server is not None:
                data[key] = self.__encode_server(server)
        buses = {b.name: self.__encode_bus(b) for b in configuration.buses}
        if len(buses):
            data['buses'] = buses
        triggers = {t.name: self.__encode_trigger(t) for t in configuration.triggers}
        if len(triggers):
            data['triggers'] = triggers

        with open(configuration.path, 'r') as f:
            comments = "".join(
                [ln for ln in f.readlines() if ln.replace(' ', '').replace('\t', '')[0] == '#']
            )

        with open(configuration.path, 'w') as f:
            f.write(yaml.dump(data))
            f.write('\n\n')
            f.write(comments)
