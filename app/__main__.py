import json
import logging
import sys
import time
import traceback
from multiprocessing import Process
from typing import List, Dict, Union, Type

from flask import Flask, send_from_directory
import os

from ledsc import LedSC

from .ledsc_fake import LedSCFake
from .triggers import TriggerMaster
from .websocket import WebSocketTask
from .events import EventQueue, EventRequest, EventExecutor, EventUpdate
from .ledsc_proxy import LedSCProxy
from .configuration import ConfigurationStructure, ConfigurationServer, ConfigurationParser, ConfigurationSlave, \
    ConfigurationBus, ConfigurationTrigger, ConfigurationConsequence, TriggerEvent
from .bus import ThreadingModbusSerialClient, BusTask
from .log import logger, LedSCLogFormatter


class CtrlTask(WebSocketTask):
    def __init__(self, server_conf: ConfigurationServer, event_queue: EventQueue,
                 triggers: List[ConfigurationTrigger]):
        super().__init__(server_conf=server_conf, event_queue=event_queue)
        self.__devices: Dict[str, LedSCProxy] = {}
        self.triggers: List[ConfigurationTrigger] = triggers

    def set_devices(self, devices: Dict[str, LedSCProxy]):
        self.__devices = devices

    @property
    def initial_message(self) -> str:
        ret = {'dev': {}, }
        for device in self.__devices.values():
            ret['dev'][device.name] = device.data_ctrl
        if len(self.triggers):
            ret['trigger'] = [t.name for t in self.triggers]
        return json.dumps(ret)


class ConfigTask(WebSocketTask):
    def __init__(self, server_conf: ConfigurationServer, event_queue: EventQueue, config: ConfigurationStructure):
        super().__init__(server_conf=server_conf, event_queue=event_queue)
        self.config = config
        self.__devices: Dict[str, LedSCProxy] = {}
        self.is_changed = False

    def set_devices(self, devices: Dict[str, LedSCProxy]):
        self.__devices = devices

    @property
    def __buses(self) -> Dict[str, dict]:
        ret = {}
        for bus_config in self.config.buses:
            ret[bus_config.name] = {
                'PATH': bus_config.path,
                'BAUD': bus_config.baud_rate,
                'PAR': bus_config.parity,
            }
        return ret

    @property
    def slaves(self) -> Dict[str, ConfigurationSlave]:
        ret = {}
        for bus_config in self.config.buses:
            ret.update({s.name: s for s in bus_config.slaves})
        return ret

    @property
    def triggers(self):
        return {t.name: t.dict for t in self.config.triggers}

    @property
    def initial_message(self) -> str:
        ret = {'is_changed': self.is_changed}
        if len(self.__buses):
            ret['bus'] = self.__buses
        if len(self.__devices):
            devs = {}
            slaves = self.slaves
            for device in self.__devices.values():
                dev_data: Dict[str, Union[str, int]] = device.data_conf
                dev_data['bus'] = slaves[device.name].bus
                devs[device.name] = dev_data
            ret['dev'] = devs
        if len(self.config.triggers):
            ret['trigger'] = self.triggers
        ret['server'] = {
            'web': {'host': self.config.server_web.host, 'port': self.config.server_web.port},
            'ctrl': {'host': self.config.server_ctrl.host, 'port': self.config.server_ctrl.port},
            'conf': {'host': self.config.server_conf.host, 'port': self.config.server_conf.port},
        }
        return json.dumps(ret)


class LedSCApp:
    def __init__(self, config: ConfigurationStructure):
        self.config = config
        self.devices: Dict[str, LedSCProxy] = {}
        self.bus_tasks: List[BusTask] = list()
        self.LedSC: Union[Type[LedSC], Type[LedSCFake]] = LedSCFake if config.virtual else LedSC

        self.event_queue_ctrl = EventQueue(enabled=config.server_ctrl is not None)
        self.event_executor_ctrl = EventExecutor(event_queue=self.event_queue_ctrl)

        self.event_queue_conf = EventQueue(enabled=config.server_conf is not None)
        self.event_executor_conf = EventExecutor(event_queue=self.event_queue_conf)

        self.app = Flask(__name__, static_folder="../www/")
        self.app.add_url_rule('/', defaults={'path': ''}, view_func=self.serve)
        self.app.add_url_rule('/<path:path>', view_func=self.serve)
        self.web_conf_path = f"{self.app.static_folder}/js/conf.mjs"

        self.ctrl_task: Union[None, CtrlTask] = None
        if config.server_ctrl is not None:
            self.ctrl_task = CtrlTask(server_conf=config.server_ctrl, event_queue=self.event_queue_ctrl,
                                      triggers=self.config.triggers)
            self.event_executor_ctrl.register_handler(event=EventUpdate, handler=self.ctrl_task.send_broadcast)
            self.event_executor_ctrl.register_handler(event=EventRequest, handler=self.on_ctrl_request)

        self.config_task: Union[None, ConfigTask] = None
        if config.server_conf is not None:
            self.config_task = ConfigTask(server_conf=config.server_conf, event_queue=self.event_queue_conf,
                                          config=config)
            self.event_executor_conf.register_handler(event=EventUpdate, handler=self.config_task.send_broadcast)
            self.event_executor_conf.register_handler(event=EventRequest, handler=self.on_config_request)

        self.trigger_master: None | TriggerMaster = None
        if len(config.triggers):
            self.__init_trigger_master()

        self.web_task: Union[None, CtrlTask] = None
        if config.server_web is not None:
            self.web_task = Process(
                target=lambda: self.app.run(host=config.server_web.host, port=config.server_web.port)
            )

    def __init_trigger_master(self):
        self.trigger_master = TriggerMaster(event_queue=self.event_queue_ctrl, devices=self.devices)
        if self.event_executor_ctrl is not None:
            self.event_executor_ctrl.register_handler(event=EventUpdate, handler=self.trigger_master.do_check)

    def on_config_request(self, request: dict):
        logger.debug(f"on_config_request: {request}")
        for category, category_data in request.items():
            category_data: dict
            if category == 'cmd':
                cmd = category_data['cmd']
                if cmd == 'save':
                    logger.info("Saving current configuration!")
                    ConfigurationParser.save(self.config)
                    self.config_task.is_changed = False
                else:
                    entity_category = category_data['category']
                    entity_name = category_data['name']
                    if entity_category not in ['dev', 'bus', 'trigger']:
                        logger.error(f"Config request {cmd}: Unsupported category: '{entity_category}'!")
                        continue
                    elif cmd == "rename":
                        new_name = category_data['new-name']
                        if ' ' in new_name or ':' in new_name or '\t' in new_name or '\n' in new_name:
                            logger.error(f"Config: Rename: new name has unsupported characters: '{new_name}'!")
                            continue
                        if entity_category == 'dev':
                            self.config_task.slaves[entity_name].name = new_name
                            logger.info(f"Config: Renamed device from '{entity_name}' to '{new_name}'!'")
                        elif entity_category == 'bus':
                            [b for b in self.config.buses if b.name == entity_name][0].name = new_name
                            logger.info(f"Config: Renamed bus from '{entity_name}' to '{new_name}'!'")
                        elif entity_category == 'trigger':
                            [b for b in self.config.triggers if b.name == entity_name][0].name = new_name
                            logger.info(f"Config: Renamed trigger from '{entity_name}' to '{new_name}'!'")
                    elif cmd == "delete":
                        if entity_category == 'dev':
                            dev = self.config_task.slaves[entity_name]
                            bus_name = dev.bus
                            [b for b in self.config.buses if b.name == bus_name][0].slaves.remove(dev)
                            logger.info(f"Config: Deleted device '{dev.name}'!")
                        elif entity_category == 'bus':
                            bus = [b for b in self.config.buses if b.name == entity_name][0]
                            for slave in bus.slaves:
                                logger.info(f"Config: Deleting device on bus '{bus.name}': '{slave.name}'!")
                            self.config.buses.remove(bus)
                            logger.info(f"Config: Deleted bus '{bus.name}'!")
                        elif entity_category == 'trigger':
                            trigger = [t for t in self.config.triggers if t.name == entity_name][0]
                            self.config.triggers.remove(trigger)
                            logger.info(f"Config: Deleted trigger '{trigger.name}'!")
                    elif cmd == "append":
                        if entity_category == 'dev':
                            bus_name = category_data.get('bus', self.config.buses[0].name)
                            slave_id = category_data.get('slave-id', 1)
                            [b for b in self.config.buses if b.name == bus_name][0].slaves.append(ConfigurationSlave(
                                name=entity_name, slave_id=slave_id, bus=bus_name
                            ))
                            logger.info(f"Config: Append device '{entity_name}'!")
                        elif entity_category == 'bus':
                            path = category_data.get('path', "")
                            baud_rate = category_data.get('baud-rate', 9600)
                            self.config.buses.append(ConfigurationBus(
                                name=entity_name, path=path, baud_rate=baud_rate, slaves=[]
                            ))
                            logger.info(f"Config: Append bus '{entity_name}'!")
                        elif entity_category == 'trigger':
                            event = TriggerEvent(category_data.get('event', "none"))
                            device = category_data.get('device', None)
                            self.config.triggers.append(ConfigurationTrigger(
                                name=entity_name, event=event, device=device, consequences=[]
                            ))
                            logger.info(f"Config: Append trigger '{entity_name}'!")
                    else:
                        logger.error(f"Config request: Unsupported command: '{cmd}'!")
                    self.config_task.is_changed = True
                self.load_bus_tasks()
                continue
            for name, data in category_data.items():
                if category == 'dev':
                    if name not in self.devices:
                        logger.warning(f"CONFIG Requested unknown device: {name}")
                        continue
                    dev = self.devices[name]

                    if 'bus' in data and data['bus'] != dev.data_conf['bus']:
                        bus_old_name = dev.data_conf['bus']
                        bus_new_name = data['bus']
                        logger.warning(f"Config request: '{name}' Change bus from '{bus_old_name}' to '{bus_new_name}'")
                        bus_old: ConfigurationBus = list(filter(lambda x: x.name == bus_old_name, self.config.buses))[0]
                        bus_new: ConfigurationBus = list(filter(lambda x: x.name == bus_new_name, self.config.buses))[0]
                        slave: ConfigurationSlave = list(filter(lambda x: x.name == dev.name, bus_old.slaves))[0]
                        bus_old.slaves.remove(slave)
                        bus_new.slaves.append(slave)
                        slave.bus = bus_new_name
                        self.config_task.is_changed = True
                        self.load_bus_tasks()

                    try:
                        dev.set_data_conf(data)
                    except Exception as E:
                        logger.error(f"Device config Error while setting '{name}':{E}")
                        if logger.level == logging.DEBUG:
                            traceback.print_exc()

                    if 'MB_ID' in data and str(data['MB_ID']) != str(dev.data_conf['MB_ID']):
                        bus_name = dev.data_conf['bus']
                        logger.warning(f"Config request: '{name}' Change slave-id"
                                       f"from '{dev.data_conf['MB_ID']}' to '{data['MB_ID']}'")
                        bus: ConfigurationBus = list(filter(lambda x: x.name == bus_name, self.config.buses))[0]
                        slave: ConfigurationSlave = list(filter(lambda x: x.name == dev.name, bus.slaves))[0]
                        slave.slave_id = int(data['MB_ID'])
                        self.config_task.is_changed = True
                        self.load_bus_tasks()
                elif category == 'bus':
                    try:
                        bus = list(filter(lambda x: x.name == name, self.config.buses))[0]
                    except IndexError:
                        logger.warning(f"CONFIG Requested unknown bus: {name}")
                        continue
                    self.config_task.is_changed = True
                    self.set_bus_config(bus, data)
                elif category == 'trigger':
                    try:
                        trigger = list(filter(lambda x: x.name == name, self.config.triggers))[0]
                    except IndexError:
                        logger.warning(f"CONFIG Requested unknown trigger: {name}")
                        continue
                    self.config_task.is_changed = True
                    self.set_trigger_config(trigger, data)
                else:
                    logger.error(f"CONFIG Requested unknown category: {category}")

    def set_trigger_config(self, trigger: ConfigurationTrigger, data: dict):
        logger.debug(f"Configuration trigger '{trigger.name}': {data}")
        is_changed = False
        if "event" in data:
            trigger.event = TriggerEvent(data["event"])
            is_changed = True
        if "device" in data:
            trigger.device = data["device"]
            is_changed = True
        if "consequences" in data:
            trigger.consequences = [ConfigurationConsequence(d) for d in data["consequences"]]
            is_changed = True
        if self.trigger_master is None:
            self.__init_trigger_master()
        self.trigger_master.triggers = self.config.triggers
        self.config_task.is_changed = is_changed
        self.config_task.send_broadcast({"trigger": {trigger.name: trigger.dict}, 'is_changed': is_changed})

    def set_bus_config(self, bus: ConfigurationBus, data: dict):
        logger.debug(f"Configuration bus '{bus.name}': {data}")
        devs_data: Dict[str, int] = {}
        if "PATH" in data:
            bus.path = data["PATH"]
        if "BAUD" in data:
            bus.baud_rate = int(data["BAUD"])
            devs_data["SER_BD"] = int(data["BAUD"])
        if "PAR" in data:
            bus.parity = data["PAR"]
            devs_data["SER_P"] = data["PAR"]
        if len(devs_data):
            for dev in [self.devices[n.name] for n in bus.slaves]:
                try:
                    dev.set_data_conf(devs_data)
                    dev.do_reset()
                except Exception as E:
                    logger.error(f"Bus config Error while setting '{dev.name}':{E}")
                    if logger.level == logging.DEBUG:
                        traceback.print_exc()
        self.load_bus_tasks()

    def on_ctrl_request(self, request: dict):
        if 'dev' in request:
            for name, data in request['dev'].items():
                if name not in self.devices:
                    logger.warning(f"ctrl Requested unknown device: {name}")
                    continue
                try:
                    self.devices[name].set_data_ctrl(data)
                except Exception as E:
                    logger.error(f"ctrl Error while setting '{name}':{E}")
                    if logger.level == logging.DEBUG:
                        traceback.print_exc()
        if 'trigger' in request:
            for name in request['trigger']:
                triggers = [t for t in self.config.triggers if t.name == name]
                if len(triggers):
                    logger.info(f"Calling trigger: '{name}'!")
                    self.trigger_master.do_trigger(triggers[0])
                else:
                    logger.error(f"Unknown trigger: '{name}'!")

    def __configure_js(self):
        text = ""
        for name, value in [("port_ctrl", self.config.server_ctrl.port), ("port_conf", self.config.server_conf.port)]:
            text += f"export let {name} = {value};\n"

        if os.path.isfile(self.web_conf_path):
            with open(self.web_conf_path, 'r') as f:
                if text == f.read():
                    logger.debug("Configuring web: No configuration change needed.")
                    return

        logger.info(f"Configuring web: Saving new configuration!")
        with open(self.web_conf_path, 'w') as f:
            f.write(text)

    def serve(self, path):
        if path == "":
            return send_from_directory(self.app.static_folder, "index.html")
        elif path != "" and os.path.exists(self.app.static_folder + "/" + path):
            return send_from_directory(self.app.static_folder, path)
        elif path != "" and os.path.exists(self.app.static_folder + "/" + path + '.html'):
            return send_from_directory(self.app.static_folder, path + '.html')
        else:
            return send_from_directory(self.app.static_folder, "404.html"), 404

    def start(self):
        if self.web_task is not None:
            logger.info(f"Starting LedSC web server on {self.config.server_web.host}:{self.config.server_web.port}")
            self.__configure_js()
            self.web_task.start()
        if self.ctrl_task is not None:
            logger.info(f"Starting LedSC ctrl on {self.config.server_ctrl.host}:{self.config.server_ctrl.port}")
            self.ctrl_task.start()
            self.event_executor_ctrl.start()
        if self.config_task is not None:
            logger.info(f"Starting LedSC CONFIG ctrl on {self.config.server_ctrl.host}:{self.config.server_ctrl.port}")
            self.config_task.start()
            self.event_executor_conf.start()
        self.load_bus_tasks()

    def load_bus_tasks(self):
        reloading = False
        if len(self.bus_tasks) > 0:
            reloading = True
            logger.warning(f"Reloading LedSC bus tasks")
            self.devices = {}
            if self.ctrl_task is not None:
                self.ctrl_task.set_devices({})
            if self.config_task is not None:
                self.config_task.set_devices({})
            if self.trigger_master is not None:
                self.trigger_master.set_devices({})
            for bus_task in self.bus_tasks:
                bus_task.killed = True
            for bus_task in self.bus_tasks:
                bus_task.join()
            self.bus_tasks = list()
        else:
            logger.info(f"Loading LedSC bus tasks")
        bus_paths: Dict[str, str] = {}
        for bus_config in self.config.buses:
            if bus_config.path in bus_paths:
                logger.error(f"Bus path '{bus_config.path}' already defined in '{bus_paths[bus_config.path]}'. "
                             f"Bus '{bus_config.name}' will be ignored!")
                continue
            bus_paths[bus_config.path] = bus_config.name
            bus = ThreadingModbusSerialClient(port=bus_config.path, baudrate=bus_config.baud_rate)
            bus_devices: List[LedSCProxy] = list()
            for slave_config in bus_config.slaves:
                driver = self.LedSC(modbus_client=bus, slave_id=slave_config.slave_id)
                slave = LedSCProxy(device_name=slave_config.name, device_driver=driver,
                                   event_queue_ctrl=self.event_queue_ctrl, event_queue_conf=self.event_queue_conf)
                bus_devices.append(slave)
                self.devices[slave_config.name] = slave
            bus_task = BusTask(bus_path=bus_config.path, devices=bus_devices, frequency=100)
            self.bus_tasks.append(bus_task)
            bus_task.start()
        if self.ctrl_task is not None:
            self.ctrl_task.set_devices(self.devices)
            if reloading:
                self.ctrl_task.send_broadcast(self.ctrl_task.initial_message)
        if self.config_task is not None:
            self.config_task.set_devices(self.devices)
            if reloading:
                self.config_task.send_broadcast(self.config_task.initial_message)
        if self.trigger_master is not None:
            self.trigger_master.set_devices(self.devices)

    def stop(self):
        logger.info(f"Stopping WebCS application...")
        for bus_task in self.bus_tasks:
            bus_task.killed = True
        if self.web_task is not None:
            self.web_task.terminate()
            self.web_task.join()
        if self.ctrl_task is not None:
            self.event_executor_ctrl.killed = True
            self.ctrl_task.stop()
            self.event_executor_ctrl.join()
            self.ctrl_task.join()
        if self.config_task is not None:
            self.event_executor_conf.killed = True
            self.config_task.stop()
            self.event_executor_conf.join()
            self.config_task.join()
        for bus_task in self.bus_tasks:
            bus_task.join()

    def run(self):
        try:
            if self.trigger_master is not None:
                time.sleep(1)
                self.trigger_master.start(self.config.triggers)
            while True:
                time.sleep(300)
                # self.load_bus_tasks()
        except KeyboardInterrupt:
            pass

        finally:
            self.stop()


def main(config: ConfigurationStructure):
    # prepare logging
    logger.setLevel(config.log_level)
    consolehandler = logging.StreamHandler(sys.stdout)
    consolehandler.setFormatter(LedSCLogFormatter())
    logger.addHandler(consolehandler)

    flask_logger = logging.getLogger("werkzeug")
    flask_logger.setLevel(logging.WARNING)

    app = LedSCApp(config)
    app.start()
    app.run()


if __name__ == "__main__":
    _config = ConfigurationParser.load("./config.yaml")
    main(config=_config)
