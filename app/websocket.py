import asyncio
import json
import logging
import threading
import traceback
from typing import List

from websockets import ConnectionClosedOK, WebSocketServerProtocol, ConnectionClosedError
from websockets.server import serve as websocket_serve

from .events import EventQueue, EventRequest
from .configuration import ConfigurationServer
from .log import logger


class WebSocketTask(threading.Thread):
    def __init__(self, server_conf: ConfigurationServer, event_queue: EventQueue):
        super().__init__()
        self.__id = type(self).__name__
        self.killed = False
        self.loop = asyncio.new_event_loop()
        self.serve = lambda: websocket_serve(ws_handler=self.__on_connect,
                                             host=server_conf.host,
                                             port=server_conf.port)
        self.connections: List[WebSocketServerProtocol] = list()
        self.event_queue = event_queue

    @property
    def initial_message(self) -> str:
        return ""

    def _on_message(self, message: dict):
        self.event_queue.add_event(EventRequest(message))

    async def __on_connect(self, connection: WebSocketServerProtocol):
        logger.info(f"{self.__id}: New websocket connection: {connection.local_address}")
        self.connections.append(connection)
        try:
            await connection.send(self.initial_message)
            while True:
                msg = await connection.recv()
                # logger.debug(f"Received message from '{connection.local_address}': {msg}")
                self._on_message(json.loads(msg))
        except ConnectionClosedOK:
            logger.info(f"{self.__id}: Websocket connection closed: {connection.local_address}")
        except ConnectionClosedError:
            logger.error(f"{self.__id}: Websocket connection broken: {connection.local_address}")
            if logger.level == logging.DEBUG:
                traceback.print_exc()
        except Exception as E:
            logger.error(f"{self.__id}: Internal error: {E}")
            if logger.level == logging.DEBUG:
                traceback.print_exc()
        finally:
            self.connections.remove(connection)

    def send_broadcast(self, data):
        # logger.debug(f"{self.__id}: sending broadcast: {data}")
        if type(data) is dict or type(data) is list:
            data = json.dumps(data)
        for connection in self.connections:
            try:
                asyncio.run(connection.send(data))
            except Exception as E:
                logger.error(f"{self.__id}: Error sending broadcast: {E}")
                if logger.level == logging.DEBUG:
                    traceback.print_exc()
                self.connections.remove(connection)

    def stop(self):
        logger.debug(f"{self.__id}: Stopping websocket thread")
        self.killed = True

    async def async_run(self):
        async with self.serve():
            while not self.killed:
                await asyncio.sleep(1)

    def run(self):
        self.loop.run_until_complete(self.async_run())
        logger.info(f"{self.__id}: Websocket thread exited")
