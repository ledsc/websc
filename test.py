import logging

from app import ConfigurationBus, ConfigurationSlave, ConfigurationServer, ConfigurationStructure
from app.__main__ import main

if __name__ == '__main__':
    buses = [
        ConfigurationBus(name="RS485_A", path="/dev/ttyUSB0", baud_rate=19200, slaves=[
            ConfigurationSlave(name="Jednicka", slave_id=1),
            ConfigurationSlave(name="Dvojka", slave_id=2),
        ]),
        ConfigurationBus(name="RS485_B", path="/dev/ttyUSB1", baud_rate=9600, slaves=[
            ConfigurationSlave(name="Ctyrka", slave_id=1),
        ]),
        ConfigurationBus(name="RS485_C", path="/dev/ttyUSB2", baud_rate=9600, slaves=[
            ConfigurationSlave(name="Petka", slave_id=1),
            ConfigurationSlave(name="Trojka", slave_id=3),
        ])
    ]

    server_web = ConfigurationServer(host="0.0.0.0", port=8080)
    server_ctrl = ConfigurationServer(host="0.0.0.0", port=8443)
    server_conf = ConfigurationServer(host="0.0.0.0", port=8444)

    _config = ConfigurationStructure(server_web=server_web, server_ctrl=server_ctrl, server_conf=server_conf, buses=buses,
                                     log_level=logging.DEBUG, path=None)
    main(config=_config)
