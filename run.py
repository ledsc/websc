from app import ConfigurationParser
from app.__main__ import main

if __name__ == '__main__':
    _config = ConfigurationParser.load("./config.yaml")
    main(config=_config)
