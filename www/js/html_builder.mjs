export class HtmlBuilder {
    static set_onclick(element, onclick) {
        if (onclick != null) {
            element.style.setProperty("cursor", "pointer")
            if (typeof onclick === "string" || onclick instanceof String) {
                element.setAttribute("onclick", onclick);
            } else {
                element.onclick = onclick;
            }
        }
    }

    static add_window(father, id) {
        let aligner = document.createElement("div");
        if (id != null) {
            aligner.id = id;
        }
        aligner.classList.add("ledsc-window-aligner");
        document.getElementById(father).appendChild(aligner)

        let window = document.createElement("div");
        window.setAttribute("class", "ledsc-window ui-content ui-body-a")
        window.setAttribute("data-role", "content")
        window.setAttribute("role", "main")
        aligner.append(window)
        return window;
    }


    static create_button(icon, onclick) {
        let btn = document.createElement("a")
        btn.setAttribute("class",
            "ui-link ui-btn ui-btn-a ui-btn-icon-notext ui-shadow ui-corner-all ledsc-btn ui-icon-" + icon)
        this.set_onclick(btn, onclick)
        return btn
    }

    static create_title_with_buttons(title, btn_left, btn_right, onclick) {
        let title_div = document.createElement("div")
        title_div.style.display = "flex";
        title_div.style.justifyContent = "space-between";
        title_div.style.alignItems = "center";
        title_div.setAttribute("data-position", "inline")

        let title_h = document.createElement("h3")
        title_h.textContent = title;
        this.set_onclick(title_h, onclick)

        title_div.append(btn_left)
        title_div.append(title_h)
        title_div.append(btn_right)
        return title_div
    }

    static create_input_with_buttons(title, btn_left, btn_right, input_id, width) {
        let title_div = document.createElement("div")
        title_div.style.display = "flex";
        title_div.style.justifyContent = "space-between";
        title_div.style.alignItems = "center";
        title_div.setAttribute("data-position", "inline")

        let _input = document.createElement("input")
        _input.setAttribute("class", "ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset")
        _input.id = input_id;
        _input.type = "text";
        _input.style.height = "35px";
        _input.style.textAlign = "center";
        _input.style.fontWeight = "bold"
        _input.style.fontSize = "20px";
        if (width != null) {
            _input.style.width = width + "px";
        }
        _input.value = title;

        title_div.append(btn_left)
        title_div.append(_input)
        title_div.append(btn_right)
        return title_div
    }

    static create_title(title, onclick) {
        let title_h = document.createElement("h3")
        title_h.textContent = title;
        this.set_onclick(title_h, onclick)

        return title_h
    }

    static add_color_picker(father, name) {
        let ui_grid = document.createElement("div");
        ui_grid.classList.add("ui-grid-a");
        father.append(ui_grid);

        let ui_block_a = document.createElement("div");
        ui_block_a.classList.add("ui-block-a");
        ui_block_a.style.setProperty("width", "82%");
        ui_grid.append(ui_block_a);


        for (let i = 0; i < 4; i++) {
            let color = ['R', 'G', 'B', 'W'][i];
            let container = document.createElement("div");
            container.classList.add("ui-field-contain");
            container.style.setProperty("width", "120%");
            container.style.setProperty("border-bottom-width", '0px');
            ui_block_a.append(container);

            let slider_id = name + "-slider-" + color;

            let label = document.createElement("label");
            label.id = name + "-slider-" + color + "-label";
            label.htmlFor = slider_id;
            label.style.setProperty("width", "1%");
            label.textContent = color + ": "
            container.append(label);

            let slider = document.createElement("input")
            slider.setAttribute("class", "ui-shadow-inset ui-corner-all ui-body-inherit")
            slider.id = slider_id
            slider.min = "0";
            slider.max = "255";
            slider.value = "0";
            container.append(slider)
            $('#' + slider.id).slider();
        }

        let ui_block_b = document.createElement("div");
        ui_block_b.classList.add("ui-block-b");
        ui_block_b.style.width = "16%"
        ui_grid.appendChild(ui_block_b)

        let color_picker_div = document.createElement("div");
        color_picker_div.setAttribute("class", "ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset")
        ui_block_b.append(color_picker_div);

        let color_picker = document.createElement("input");
        color_picker.classList.add("color_picker");
        color_picker.setAttribute("data-theme", "a")
        color_picker.type = "color";
        color_picker.id = name + "-color_picker";
        color_picker.value = "#000000";
        color_picker_div.append(color_picker);
    }

    static add_color_channels(father, name) {
        let options = {
            0: "Channel A",
            1: "Channel B",
            2: "Channel C",
            3: "Channel D",
        }

        for (let i = 0; i < 4; i++) {
            let color = ['R', 'G', 'B', 'W'][i];
            let container = document.createElement("div");
            container.classList.add("ui-field-contain");
            container.style.setProperty("width", "120%");
            container.style.setProperty("border-bottom-width", '0px');
            container.style.setProperty("align-items", 'baseline');
            father.append(container);

            let color_id = name + "-channel-" + color;

            let label = document.createElement("label");
            label.id = name + "-channel-" + color + "-label";
            label.htmlFor = color_id;
            label.style.setProperty("width", "1%");
            label.textContent = color + ": "
            container.append(label);

            let selector = document.createElement("select")
            selector.className = "ui-btn ui-icon-carat-d ui-btn-icon-right ui-btn-a ui-corner-all ui-shadow"
            selector.id = color_id;
            selector.name = color_id;
            for (let [key, value] of Object.entries(options)) {
                let option = document.createElement("option");
                option.value = key;
                option.text = value;
                selector.appendChild(option);
            }
            container.append(selector);
        }
    }

    static create_label(for_id, title){
        let _label = document.createElement("label");
        _label.id = for_id + "-label";
        _label.htmlFor = for_id;
        _label.textContent = title + ": "
        return _label;
    }

    static create_container(container_id) {
        let _container = document.createElement("div");
        _container.classList.add("ui-field-contain");
        _container.id = container_id;
        _container.style.setProperty("align-items", 'baseline');
        _container.style.setProperty("border-bottom-width", '0px');
        return _container
    }

    static create_selector(selector_id, options, width) {
        let _selector = document.createElement("select")
        _selector.className = "ui-btn ui-icon-carat-d ui-btn-icon-right ui-btn-a ui-corner-all ui-shadow"
        _selector.id = selector_id;
        _selector.name = selector_id;
        for (let [key, value] of Object.entries(options)) {
            let option = document.createElement("option");
            option.value = key;
            option.text = value;
            _selector.appendChild(option);
        }
        if (width != null) {
            _selector.style.width = width + "px";
        }
        return _selector
    }

    static add_switch(father, label, switch_id){
        let _container = document.createElement("div");
        _container.classList.add("ui-field-contain");
        _container.style.setProperty("border-bottom-width", '0px');
        _container.style.setProperty("align-items", 'baseline');
        father.append(_container);

        let _label = document.createElement("label");
        _label.id = switch_id + "-label";
        _label.htmlFor = switch_id;
        _label.textContent = label + ": ";
        _label.style.setProperty("width", "200px");
        _container.append(_label);

        let _switch = document.createElement("input");
        _switch.setAttribute("class", "ui-shadow-inset ui-corner-all ui-body-inherit");
        _switch.id = switch_id;
        _container.append(_switch);
        $('#' + _switch.id).flipswitch();
        return _switch;
    }

    static add_number_input(father, label, input_id, min, max, width, label_width){
        let _container = document.createElement("div");
        _container.classList.add("ui-field-contain");
        _container.style.setProperty("border-bottom-width", '0px');
        _container.style.setProperty("display", 'flex');
        _container.style.setProperty("align-items", 'baseline');
        father.append(_container);

        let _label = document.createElement("label");
        _label.id = input_id + "-label";
        _label.htmlFor = input_id;
        _label.textContent = label + ": ";
        if (label_width != null) {
            _label.style.width = label_width + "px";
        }
        _container.append(_label);

        let _input = document.createElement("input")
        _input.setAttribute("class", "ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset")
        _input.id = input_id;
        _input.type = "number";
        _input.style.height = "35px";
        if (width != null) {
            _input.style.width = width + "px";
        }
        _input.min = min;
        _input.max = max;
        _container.append(_input);
        return _input;
    }

    static add_string_input(father, label, input_id, width){
        let _container = document.createElement("div");
        _container.classList.add("ui-field-contain");
        _container.style.setProperty("border-bottom-width", '0px');
        _container.style.setProperty("display", 'flex');
        _container.style.setProperty("align-items", 'baseline');
        father.append(_container);

        let _label = document.createElement("label");
        _label.id = input_id + "-label";
        _label.htmlFor = input_id;
        _label.textContent = label + ": ";
        _label.style.setProperty("width", "200px");
        _container.append(_label);

        let _input = document.createElement("input")
        _input.setAttribute("class", "ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset")
        _input.id = input_id;
        _input.type = "text";
        _input.style.height = "35px";
        if (width != null) {
            _input.style.width = width + "px";
        }
        _container.append(_input);
        return _input;
    }

    static add_selector(father, label, selector_id, options, width, label_width){
        let _container = document.createElement("div");
        _container.classList.add("ui-field-contain");
        _container.style.setProperty("border-bottom-width", '0px');
        _container.style.setProperty("display", 'flex');
        _container.style.setProperty("align-items", 'baseline');
        father.append(_container);

        if (label != null) {
            let _label = document.createElement("label");
            _label.id = selector_id + "-label";
            _label.htmlFor = selector_id;
            _label.textContent = label + ": ";
            if (label_width != null) {
                _label.style.width = label_width + "px";
            }
            _container.append(_label);
        }

        let _selector = HtmlBuilder.create_selector(selector_id, options, width)
        _container.append(_selector);
        return _selector;
    }
}
