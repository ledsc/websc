import {HtmlBuilder} from "./html_builder.mjs";

export class ServerApi {
    constructor(socket) {
        this.codename = "API"
        this.name = "Server-API-configuration"
        this.socket = socket;
        this.windows = [];
        this.changed = false;
        this.save_ready = false;
    }

    set_changed = (is_changed) => {
        let changed = this.changed;

        this.save_ready = is_changed;
        if(this.save_ready){
            this.set_state("EXT");
        } else {
            this.set_state("CLEAN");
        }
    }

    set_ctrl = (data) => {
        let changed = this.changed;

        if('host' in data) this.ctrl_host.val(data['host']);
        if('port' in data) this.ctrl_port.val(data['port']);

        if (changed){
            this.set_state("ERR")
        } else if (this.save_ready) {
            this.set_state("EXT");
        } else {
            this.set_state("CLEAN");
        }
    }

    set_conf = (data) => {
        let changed = this.changed;

        if('host' in data) this.conf_host.val(data['host']);
        if('port' in data) this.conf_port.val(data['port']);

        if (changed){
            this.set_state("ERR")
        } else if (this.save_ready) {
            this.set_state("EXT");
        } else {
            this.set_state("CLEAN");
        }
    }

    __initialize() {
        this.ctrl_host = $("#" + this.name + "-ctrl-host");
        this.ctrl_port = $("#" + this.name + "-ctrl-port");
        this.conf_host = $("#" + this.name + "-conf-host");
        this.conf_port = $("#" + this.name + "-conf-port");
        this.btn_save = $("#" + this.name + "-btn-save");
    }

    set_state = (state) => {
        this.btn_save.removeClass("ui-icon-edit")
        this.btn_save.removeClass("ui-icon-check")
        switch(state) {
            case "CLEAN":
                this.btn_save.css("background-color", '');
                this.btn_save.addClass("ui-icon-check")
                break;
            case "LOCAL":
                this.btn_save.css("background-color", 'orange');
                this.btn_save.addClass("ui-icon-check")
                this.changed = true;
                break
            case "EXT":
                this.btn_save.css("background-color", 'green');
                this.btn_save.addClass("ui-icon-edit")
                this.changed = false;
                break
            case "ERR":
                this.btn_save.css("background-color", 'orange');
                this.btn_save.addClass("ui-icon-alert")
                break
        }
    }

    show() {
        for (let window of this.windows) {
            window.show()
        }
    }

    hide = () => {
        for (let window of this.windows) {
            window.hide()
        }
    }

    reload = () => {
        console.log(this.name + ": reloading...");
    }

    save = () => {
        if ( Object.keys(this.changed).length ) {
            let msg = {'server': {}}
            msg['server'][this.codename] = this.changed;
            this.socket.send(JSON.stringify(msg));
            this.changed = {};
        } else if (this.save_ready) {
            this.socket.send(JSON.stringify({'cmd': {'cmd': 'save'}}));
        }
    }

    create_html() {
        let window_head = HtmlBuilder.add_window("dashboard", this.name + '-window-head');
        let btn_reload = HtmlBuilder.create_button("refresh", this.reload);
        let btn_save = HtmlBuilder.create_button("check", this.save);
        btn_save.id = this.name + "-btn-save";
        let title_head = HtmlBuilder.create_title_with_buttons(this.name.replace("-", " "), btn_reload, btn_save, null);
        window_head.append(title_head);
        this.windows.push($('#' + this.name + '-window-head'));

        // ------------------------------------------

        let window_ctrl = HtmlBuilder.add_window("dashboard", this.name + '-window-ctrl');
        let title_ctrl = HtmlBuilder.create_title("Control API", null);
        window_ctrl.append(title_ctrl);
        HtmlBuilder.add_string_input(window_ctrl, "Host", this.name + '-ctrl-host')
        HtmlBuilder.add_number_input(window_ctrl, "Port", this.name + '-ctrl-port')
        this.windows.push($('#' + this.name + '-window-ctrl'));

        let window_conf = HtmlBuilder.add_window("dashboard", this.name + '-window-conf');
        let title_conf= HtmlBuilder.create_title("Configuration API", null);
        window_conf.append(title_conf);
        HtmlBuilder.add_string_input(window_conf, "Host", this.name + '-conf-host')
        HtmlBuilder.add_number_input(window_conf, "Port", this.name + '-conf-port')
        this.windows.push($('#' + this.name + '-window-conf'));

        // ------------------------------------------

        this.__initialize()
    }
}

export class ServerWeb extends ServerApi {
    constructor(socket) {
        super(socket);
        this.codename = "API"
        this.name = "Server-WEB-configuration"
    }

    set_changed = (is_changed) => {
        this.save_ready = is_changed;
        if(this.save_ready){
            this.set_state("EXT");
        } else {
            this.set_state("CLEAN");
        }
    }

    set = (data) => {
        let changed = this.changed;

        if('host' in data) this.host.val(data['host']);
        if('port' in data) this.port.val(data['port']);

        if (changed){
            this.set_state("ERR")
        } else if (this.save_ready) {
            this.set_state("EXT");
        } else {
            this.set_state("CLEAN");
        }
    }

    __initialize() {
        this.host = $("#" + this.name + "-host");
        this.port = $("#" + this.name + "-port");
        this.btn_save = $("#" + this.name + "-btn-save");
    }

    create_html() {
        let window_head = HtmlBuilder.add_window("dashboard", this.name + '-window-head');
        let btn_reload = HtmlBuilder.create_button("refresh", this.reload);
        let btn_save = HtmlBuilder.create_button("check", this.save);
        btn_save.id = this.name + "-btn-save";
        let title_head = HtmlBuilder.create_title_with_buttons(this.name.replace("-", " "), btn_reload, btn_save, null);
        window_head.append(title_head);
        this.windows.push($('#' + this.name + '-window-head'));

        // ------------------------------------------

        let window_web = HtmlBuilder.add_window("dashboard", this.name + '-window-web');
        let title_web = HtmlBuilder.create_title("Web application", null);
        window_web.append(title_web);
        HtmlBuilder.add_string_input(window_web, "Host", this.name + '-host')
        HtmlBuilder.add_number_input(window_web, "Port", this.name + '-port')
        this.windows.push($('#' + this.name + '-window-web'));

        // ------------------------------------------

        this.__initialize()
    }
}
