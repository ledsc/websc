import {HtmlBuilder} from './html_builder.mjs'

var hexDigits =["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"];
//import { io } from "socket.io.min.js";

function hex(x) {
    return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
}

function constrain(x, minimal, maximal){
    if (x < minimal) return minimal;
    else if (x > maximal) return maximal;
    return x;
}

function rgbw2hex(rgbw) {
    let w = rgbw[3];
    let r = constrain(rgbw[0]+w, 0 , 255);
    let g = constrain(rgbw[1]+w, 0 , 255);
    let b = constrain(rgbw[2]+w, 0 , 255);
    return "#" + hex(r) + hex(g) + hex(b);
}

function hex2rgbw(hex){
    return [
        Number("0x"+hex.slice(1, 3)),
        Number("0x"+hex.slice(3, 5)),
        Number("0x"+hex.slice(5, 7)),
        0
    ]
}

function check_new_name(new_name) {
    new_name = new_name.replace(/ /g, '-');
    new_name = new_name.replace(/:/g, '-');
    return new_name
}


export class LedSCDashboard {
    constructor(name, socket) {
        this.name = name;
        this.socket = socket
        this.events_enabled = true;
        this.pwm_cycle = 255;
    }

    __initialize() {
        this.slider_R = $("#" + this.name + "-slider-R")
        this.slider_G = $("#" + this.name + "-slider-G")
        this.slider_B = $("#" + this.name + "-slider-B")
        this.slider_W = $("#" + this.name + "-slider-W")
        this.color_picker = $("#" + this.name + "-color_picker")

        let self = this;
        this.slider_R.on("change", function () {self.on_slider();});
        this.slider_G.on("change", function () {self.on_slider();});
        this.slider_B.on("change", function () {self.on_slider();});
        this.slider_W.on("change", function () {self.on_slider();});
        this.color_picker.on("change", function () {self.on_color_picker();});
    }

    to8bit(value){
        return Math.round(value/255*this.pwm_cycle);
    }

    from8bit(value){
        return Math.round(value/this.pwm_cycle*255);
    }

    get_rgbw = () => {
        return [
            this.to8bit(this.slider_R.val()),
            this.to8bit(this.slider_G.val()),
            this.to8bit(this.slider_B.val()),
            this.to8bit(this.slider_W.val()),
        ]
    }

    get_status_data(rgbw){
        let ret = {}
        ret['dev'] = {}
        ret['dev'][this.name] = {
            "R": rgbw[0],
            "G": rgbw[1],
            "B": rgbw[2],
            "W": rgbw[3],
        }
        return JSON.stringify(ret)
    }

    on_slider(){
        let rgbw = this.get_rgbw();
        let value = rgbw2hex(rgbw)
        //console.log("Slider change event " + value + " -> " + this.color_picker)
        this.color_picker.val(value);
        if(this.events_enabled) this.socket.send(this.get_status_data(rgbw));
    }

    set = (data) => {
        let rgbw = this.get_rgbw();
        if('R' in data) rgbw[0] = data['R'];
        if('G' in data) rgbw[1] = data['G'];
        if('B' in data) rgbw[2] = data['B'];
        if('W' in data) rgbw[3] = data['W'];
        if('PWMC' in data) this.pwm_cycle = data['PWMC'];
        this.__set_rgbw(rgbw)
    }

    on_color_picker(){
        let rgbw = hex2rgbw(this.color_picker.val());
        if(this.events_enabled) this.socket.send(this.get_status_data(rgbw));
        this.__set_rgbw(rgbw)
    }

    __set_rgbw(rgbw){
        this.events_enabled = false;
        this.slider_R.val(this.from8bit(rgbw[0])).trigger("change");
        this.slider_G.val(this.from8bit(rgbw[1])).trigger("change");
        this.slider_B.val(this.from8bit(rgbw[2])).trigger("change");
        this.slider_W.val(this.from8bit(rgbw[3])).trigger("change");
        this.events_enabled = true;
    }

    trigger = () => {
        let msg = {};
        msg['dev'] = {};
        msg['dev'][this.name] = {'trigger': true};
        this.socket.send(JSON.stringify(msg));
    }

    create_html() {
        let window = HtmlBuilder.add_window("dashboard");
        
        let btn_trigger = HtmlBuilder.create_button("power", this.trigger);
        let btn_conf = HtmlBuilder.create_button("gear",
            "window.location.href='configuration#dev#"+this.name+"'");
        let head = HtmlBuilder.create_title_with_buttons(this.name, btn_trigger, btn_conf,
            "window.location.href='control#"+this.name+"'", );
        window.append(head);
        HtmlBuilder.add_color_picker(window, this.name);

        this.__initialize()
    }
}


export class LedSCConfig {
    constructor(name, socket, buses) {
        this.name = name;
        this.socket = socket;
        this.windows = [];
        this.changed = false;
        this.save_ready = false;
        this.update_buses(buses)
        this.deleted = false;
    }

    __initialize(){
        this.slider_def_R = $("#" + this.name + "-def-slider-R")
        this.slider_def_G = $("#" + this.name + "-def-slider-G")
        this.slider_def_B = $("#" + this.name + "-def-slider-B")
        this.slider_def_W = $("#" + this.name + "-def-slider-W")
        this.color_picker = $("#" + this.name + "-def-color_picker")
        this.ch_cnf_R = $("#" + this.name + "-channel-R")
        this.ch_cnf_G = $("#" + this.name + "-channel-G")
        this.ch_cnf_B = $("#" + this.name + "-channel-B")
        this.ch_cnf_W = $("#" + this.name + "-channel-W")
        this.slave_id = $("#" + this.name + "-in-slave_id")
        this.def_pwm_cycle = $("#" + this.name + "-in-df_pwmc");
        this.def_transition_time = $("#" + this.name + "-in-df_tt");
        this.en_proximitty = $("#" + this.name + '-switch-en_px');
        this.en_greetings = $("#" + this.name + '-switch-en_gr');
        this.en_light_on_start = $("#" + this.name + '-switch-en_los');
        this.bus = $("#" + this.name + '-bus');
        this.btn_save = $("#" + this.name + "-btn-save");
        this.in_name = $("#" + this.name + "-name");

        let self = this;
        this.slider_def_R.on("change", function () {self.on_def_slider(); self.set_state("LOCAL")});
        this.slider_def_G.on("change", function () {self.on_def_slider(); self.set_state("LOCAL")});
        this.slider_def_B.on("change", function () {self.on_def_slider(); self.set_state("LOCAL")});
        this.slider_def_W.on("change", function () {self.on_def_slider(); self.set_state("LOCAL")});
        this.color_picker.on("change", function () {self.on_def_color_picker()});
        this.ch_cnf_R.on("change", function () {self.check_channel_configuration(); self.set_state("LOCAL")});
        this.ch_cnf_G.on("change", function () {self.check_channel_configuration(); self.set_state("LOCAL")});
        this.ch_cnf_B.on("change", function () {self.check_channel_configuration(); self.set_state("LOCAL")});
        this.ch_cnf_W.on("change", function () {self.check_channel_configuration(); self.set_state("LOCAL")});
        this.slave_id.on("change", function () {self.set_state("LOCAL")});
        this.def_pwm_cycle.on("change", function () {self.set_state("LOCAL")});
        this.def_transition_time.on("change", function () {self.set_state("LOCAL")});
        this.en_proximitty.on("change", function () {self.set_state("LOCAL")});
        this.en_greetings.on("change", function () {self.set_state("LOCAL")});
        this.en_light_on_start.on("change", function () {self.set_state("LOCAL")});
        this.bus.on("change", function () {self.set_state("LOCAL")});
        this.in_name.on("change", function(){self.changed = true; self.set_state("LOCAL")})
    }

    to8bit(value){
        return Math.round(value/255*this.def_pwm_cycle.val());
    }

    from8bit(value){
        return Math.round(value/this.def_pwm_cycle.val()*255);
    }

    update_buses = (buses) => {
        this.buses = {}
        for ( const [name, bus] of Object.entries(buses)) {
            this.buses[name] = name;
        }
    }

    set_changed = (is_changed) => {
        this.save_ready = is_changed;
        if(this.save_ready){
            this.set_state("EXT");
        } else {
            this.set_state("CLEAN");
        }
    }

    set = (data) => {
        let changed = this.changed;

        let change_lch = false;
        let change_df_rgbw = false;
        let rgbw = this.get_def_rgbw();
        if('DF_R' in data) rgbw[0] = data['DF_R']; change_df_rgbw = true;
        if('DF_G' in data) rgbw[1] = data['DF_G']; change_df_rgbw = true;
        if('DF_B' in data) rgbw[2] = data['DF_B']; change_df_rgbw = true;
        if('DF_W' in data) rgbw[3] = data['DF_W']; change_df_rgbw = true;
        if('DF_PWMC' in data) this.def_pwm_cycle.val(data['DF_PWMC']);
        if('LCH_R' in data) this.ch_cnf_R.val(data['LCH_R']); change_lch = true;
        if('LCH_G' in data) this.ch_cnf_G.val(data['LCH_G']); change_lch = true;
        if('LCH_B' in data) this.ch_cnf_B.val(data['LCH_B']); change_lch = true;
        if('LCH_W' in data) this.ch_cnf_W.val(data['LCH_W']); change_lch = true;
        if('MB_ID' in data) this.slave_id.val(data['MB_ID']);
        if('DF_TT' in data) this.def_transition_time.val(data['DF_TT']);
        if('PX_EN' in data) this.en_proximitty.prop('checked', data['PX_EN']).flipswitch('refresh');
        if('GR_EN' in data) this.en_greetings.prop('checked', data['GR_EN']).flipswitch('refresh');
        if('LOS' in data) this.en_light_on_start.prop('checked', data['LOS']).flipswitch('refresh');
        if('bus' in data) this.bus.val(data['bus']);
        if ( change_lch ) this.check_channel_configuration();
        if ( change_df_rgbw ) this.__set_def_rgbw(rgbw);

        if (changed){
            this.set_state("ERR")
        } else if (this.save_ready) {
            this.set_state("EXT");
        } else {
            this.set_state("CLEAN");
        }
    }

    set_state = (state) => {
        this.btn_save.removeClass("ui-icon-edit")
        this.btn_save.removeClass("ui-icon-check")
        switch(state) {
            case "CLEAN":
                this.btn_save.css("background-color", '');
                this.btn_save.addClass("ui-icon-check")
                break;
            case "LOCAL":
                this.btn_save.css("background-color", 'orange');
                this.btn_save.addClass("ui-icon-check")
                this.changed = true;
                break
            case "EXT":
                this.btn_save.css("background-color", 'green');
                this.btn_save.addClass("ui-icon-edit")
                this.changed = false;
                break
            case "ERR":
                this.btn_save.css("background-color", 'orange');
                this.btn_save.addClass("ui-icon-alert")
                break
        }
    }

    on_def_slider(){
        let rgbw = this.get_def_rgbw();
        let value = rgbw2hex(rgbw)
        //console.log("Slider change event " + value + " -> " + this.color_picker)
        this.color_picker.val(value);
    }


    on_def_color_picker(){
        let rgbw = hex2rgbw(this.color_picker.val());
        this.__set_def_rgbw(rgbw)
    }

    get_def_rgbw = () => {
        return [
            this.to8bit(this.slider_def_R.val()),
            this.to8bit(this.slider_def_G.val()),
            this.to8bit(this.slider_def_B.val()),
            this.to8bit(this.slider_def_W.val()),
        ]
    }

    __set_def_rgbw = (rgbw)=> {
        this.slider_def_R.val(this.from8bit(rgbw[0])).trigger("change");
        this.slider_def_G.val(this.from8bit(rgbw[1])).trigger("change");
        this.slider_def_B.val(this.from8bit(rgbw[2])).trigger("change");
        this.slider_def_W.val(this.from8bit(rgbw[3])).trigger("change");
    }

    check_channel_configuration = () => {
        let selected = {};
        for (let ch of [this.ch_cnf_R, this.ch_cnf_G, this.ch_cnf_B, this.ch_cnf_W]) {
            let v = ch.val();
            if ( v in selected ){
                selected[v].css("background-color", 'red');
                ch.css("background-color", 'red');
            } else {
                selected[v] = ch;
                ch.css("background-color", '');
            }
        }
    }

    delete = () => {
        if (! this.deleted) {
            this.socket.send(JSON.stringify({'cmd': {'cmd': 'delete', 'category': 'dev', 'name': this.name}}));
            this.hide()
            this.windows[0].show();
            this.deleted = true;
        }
    }

    save = () => {
        if ( this.changed ) {  // TODO: posilat pouze zmeny?
            this.set_state("CLEAN")
            let msg = {'dev': {}}
            msg['dev'][this.name] = {
                "DF_R": this.to8bit(this.slider_def_R.val()),
                "DF_G": this.to8bit(this.slider_def_G.val()),
                "DF_B": this.to8bit(this.slider_def_B.val()),
                "DF_W": this.to8bit(this.slider_def_W.val()),
                'LCH_R': this.ch_cnf_R.val(),
                'LCH_G': this.ch_cnf_G.val(),
                'LCH_B': this.ch_cnf_B.val(),
                'LCH_W': this.ch_cnf_W.val(),
                'DF_PWMC': this.def_pwm_cycle.val(),
                'MB_ID': this.slave_id.val(),
                'DF_TT': this.def_transition_time.val(),
                'PX_EN': this.en_proximitty.prop('checked'),
                'GR_EN': this.en_greetings.prop('checked'),
                'LOS': this.en_light_on_start.prop('checked'),
                'bus': this.bus.val(),
            }
            this.socket.send(JSON.stringify(msg));
            if (this.name !== this.in_name.val()) {
                let new_name = check_new_name(this.in_name.val());
                let req = {'cmd': {'cmd': 'rename', 'category': 'dev'}};
                req['cmd']['name'] = this.name;
                req['cmd']['new-name'] = new_name;
                this.socket.send(JSON.stringify(req));
                window.location.hash = "#dev#" + new_name;
                location.reload()
            }
        } else if (this.save_ready) {
            this.socket.send(JSON.stringify({'cmd': {'cmd': 'save'}}));
            if (this.deleted) {
                window.location.hash = "";
                location.reload()
            }
        }
    }

    show(){
        for (let window of this.windows){
            window.show()
        }
    }

    hide = () => {
        for (let window of this.windows){
            window.hide()
        }
    }

    create_html() {
        let window_head = HtmlBuilder.add_window("dashboard", this.name + '-window-head');
        let btn_delete = HtmlBuilder.create_button("delete", this.delete);
        let btn_save = HtmlBuilder.create_button("check",this.save);
        btn_save.id = this.name + "-btn-save";
        let title_head = HtmlBuilder.create_input_with_buttons(this.name, btn_delete, btn_save,this.name+"-name");
        window_head.append(title_head);
        this.windows.push($('#'+this.name + '-window-head'));

        // ------------------------------------------

        let window_modbus = HtmlBuilder.add_window("dashboard", this.name + '-window-modbus');
        let title_modbus = HtmlBuilder.create_title("Modbus settings", null);
        window_modbus.append(title_modbus);
        HtmlBuilder.add_number_input(window_modbus, "Modbus address", this.name+'-in-slave_id',
            0, 65536);
        HtmlBuilder.add_selector(window_modbus, "Bus", this.name+"-bus", this.buses)
        this.windows.push($('#'+this.name + '-window-modbus'));

        // ------------------------------------------

        let window_def_color = HtmlBuilder.add_window("dashboard", this.name + '-window-def-color');
        let title_def_color = HtmlBuilder.create_title("Default color", null);
        window_def_color.append(title_def_color);
        HtmlBuilder.add_color_picker(window_def_color, this.name+"-def");
        this.windows.push($('#'+this.name + '-window-def-color'));

        // ------------------------------------------

        let window_channels = HtmlBuilder.add_window("dashboard", this.name + '-window-channels');
        let title_channels = HtmlBuilder.create_title("Channel configuration", null);
        window_channels.append(title_channels);
        HtmlBuilder.add_color_channels(window_channels, this.name);
        this.windows.push($('#'+this.name + '-window-channels'));

        // ------------------------------------------

        let window_others = HtmlBuilder.add_window("dashboard", this.name + '-window-others');
        let title_others = HtmlBuilder.create_title("Others", null);
        window_others.append(title_others);

        let ui_grid = document.createElement("div");
        ui_grid.classList.add("ui-grid-a");
        window_others.append(ui_grid);

        let ui_block_a = document.createElement("div");
        ui_block_a.classList.add("ui-block-a");
        ui_grid.append(ui_block_a);
        HtmlBuilder.add_switch(ui_block_a, "Enable proximity", this.name+'-switch-en_px');
        HtmlBuilder.add_switch(ui_block_a, "Enable Greetings", this.name+'-switch-en_gr');
        HtmlBuilder.add_switch(ui_block_a, "Light on start", this.name+'-switch-en_los');

        let ui_block_b = document.createElement("div");
        ui_block_b.classList.add("ui-block-b");
        ui_grid.appendChild(ui_block_b)
        HtmlBuilder.add_number_input(ui_block_b, "Default Transition time", this.name+'-in-df_tt', 0, 65536, 100);
        HtmlBuilder.add_number_input(ui_block_b, "Default PWM cycle", this.name+'-in-df_pwmc', 0, 65536, 100);
        this.windows.push($('#'+this.name + '-window-others'));

        // ------------------------------------------

        this.__initialize()
    }

}
