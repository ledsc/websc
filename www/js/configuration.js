import {LedSCConfig} from './ledSC.mjs';
import { port_conf } from "./conf.mjs"
import {ModbusBus} from "./bus.mjs";
import {ServerApi, ServerWeb} from "./servers.mjs";
import {Trigger} from "./trigger.mjs";

let category_selector = $("#category-selector")


let host = $(location).attr("hostname")
let socket_conf = new WebSocket("ws://" + host + ":"+port_conf);
let is_initial_msg = true;

let __data = {
    'server': [],
    'dev': [],
    'bus': [],
    'trigger': [],
}

socket_conf.onclose = (data) => {
    if (data.wasClean) {
        console.log("Configuration WS Connection closed");
    } else {
        console.log("Configuration WS Connection broken");
    }
    //console.log('code: ' + data.code + ' reason: ' + data.reason);
};

socket_conf.onerror = (error) => {
    console.log("Configuration WS Connection error")
}

socket_conf.onopen = (data) => {
    console.log("Configuration WS Connection opened");
    let server_api = new ServerApi(socket_conf);
    server_api.create_html();
    __data['server']['API'] = server_api;
    let server_web = new ServerWeb(socket_conf);
    server_web.create_html();
    __data['server']['WEB'] = server_web;
};

socket_conf.onmessage = (message) => {
    if(message.data.length === 0) return;
    do_request(JSON.parse(message.data));
}

function do_request (data) {
    //console.log(data)
    let buses_changed = false;
    for (const [category, values] of Object.entries(data)) {
        if(category === "dev") for(const [key, value] of Object.entries(values)){
            if(key in __data['dev']){
                __data['dev'][key].set(value);
            } else {
                let device = new LedSCConfig(key, socket_conf, __data['bus']);
                __data['dev'][key] = device;
                device.create_html()
                device.set(value)
                __load_target();
            }
        }
        else if(category === "bus") for(const [key, value] of Object.entries(values)){
            if(key in __data['bus']){
                __data['bus'][key].set(value);
            } else {
                let bus = new ModbusBus(key, socket_conf);
                __data['bus'][key] = bus;
                bus.create_html()
                bus.set(value)
                __load_target();
                buses_changed = true;
            }
        }
        else if(category === "trigger") for(const [key, value] of Object.entries(values)){
            if(key in __data['trigger']){
                __data['trigger'][key].set(value);
            } else {
                let trigger = new Trigger(key, socket_conf, __data['dev']);
                __data['trigger'][key] = trigger;
                trigger.create_html()
                trigger.set(value)
                __load_target();
                buses_changed = true;
            }
        }
        else if(category === "server") for(const [key, value] of Object.entries(values)){
            if(key === 'ctrl') {
                __data['server']['API'].set_ctrl(value);
            } else if(key === 'conf'){
                __data['server']['API'].set_conf(value);
            } else if(key === 'web'){
                __data['server']['WEB'].set(value);
            } else {
                console.error("Unknown server: " + key)
            }
        }
    }
    if (buses_changed){
        for ( const [name, dev] of Object.entries(__data['dev'])){
            dev.update_buses(__data['bus']);
        }
    }
    if(is_initial_msg){
        let category = window.location.hash.substring(1).split('#')[0];
        if (!(category && category in __data)) category = 'dev';
        category_selector.val(category).trigger("change");
        __load_target();
        is_initial_msg = false;
    }
    if("is_changed" in data) {
        for(let [category, _] of Object.entries(__data)){
            for(let [key, _] of Object.entries(__data[category])){
                __data[category][key].set_changed(data['is_changed'])
            }
        }
    }
}


let panel_items = [];
category_selector.on("change", function(){
    let selector_list = $("#selector-list");

    for ( let item of panel_items ) {
        $(item).remove();
    }
    panel_items = [];

    for ( const [key, value] of Object.entries(__data[this.value]) ) {
        let item_label = document.createElement("a");
        item_label.setAttribute("class", "ui-btn ui-btn-icon-right ui-icon-carat-r")
        item_label.setAttribute("onclick", "window.location.href='#"+ this.value +"#" + key + "'")
        item_label.textContent = key;
        let item = document.createElement("li");
        item.append(item_label);
        selector_list.append(item);
        panel_items.push(item)
    }

    if(this.value === "server"){
        $("#btn-add-element").hide()
    } else {
        $("#btn-add-element").show()
    }

})

$("#btn-add-element").on("click", function(){
    let category = category_selector.val();
    let name = "";
    let i = 1;
    do {name = "new-" + category + '-' + i++} while (name in __data[category]);

    let req = {'cmd': {'cmd': 'append'}};
    req['cmd']['category'] = category;
    req['cmd']['name'] = name;
    socket_conf.send(JSON.stringify(req));

    window.location.hash = category + "#" + name;
    setTimeout(function () {
        category_selector.trigger("change");
    }, 100);
})

function hide_all(){
    for(let [category, _] of Object.entries(__data)){
        for(let [key, _] of Object.entries(__data[category])){
            __data[category][key].hide()
        }
    }
}

function __load_target() {
    hide_all();
    let fragment = window.location.hash;
    if (fragment) {
        fragment = fragment.substring(1).split("&")[0];
        var [category, key] = fragment.split("#");
        if (category && key) {
            if (category in __data && key in __data[category]) {
                __data[category][key].show();
            }
        }
    }
}

document.addEventListener("DOMContentLoaded", __load_target);
window.addEventListener("hashchange", __load_target);
