import {HtmlBuilder} from "./html_builder.mjs";

function check_new_name(new_name) {
    new_name = new_name.replace(/ /g, '-');
    new_name = new_name.replace(/:/g, '-');
    return new_name
}

export class ModbusBus {
    constructor(name, socket) {
        this.name = name;
        this.socket = socket;
        this.windows = [];
        this.changed = {};
        this.save_ready = false;
        this.deleted = false;
    }

    set_changed = (is_changed) => {
        this.save_ready = is_changed;
        if(this.save_ready){
            this.set_state("EXT");
        } else {
            this.set_state("CLEAN");
        }
    }

    set = (data) => {
        let changed = this.changed;

        if('PATH' in data) this.serial_path.val(data['PATH']);
        if('BAUD' in data) this.serial_baud.val(data['BAUD']);
        if('PAR' in data) this.serial_parity.val(data['PAR']);

        if ( Object.keys(changed).length ) {
            this.set_state("ERR")
        } else if (this.save_ready) {
            this.set_state("EXT");
        } else {
            this.set_state("CLEAN");
        }
    }

    __initialize() {
        this.serial_path = $("#" + this.name + "-serial-path")
        this.serial_baud = $("#" + this.name + "-serial-baud")
        this.serial_parity = $("#" + this.name + "-serial-parity")
        this.btn_save = $("#" + this.name + "-btn-save");
        this.in_name = $("#" + this.name + "-name");

        let self = this;
        this.serial_path.on("change", function(){self.changed["PATH"] = self.serial_path.val(); self.set_state("LOCAL")})
        this.serial_baud.on("change", function(){self.changed["BAUD"] = self.serial_baud.val(); self.set_state("LOCAL")})
        this.serial_parity.on("change", function(){self.changed["PAR"] = self.serial_parity.val(); self.set_state("LOCAL")})
        this.in_name.on("change", function(){self.changed["NAME"] = true; self.set_state("LOCAL")})
    }

    set_state = (state) => {
        this.btn_save.removeClass("ui-icon-edit")
        this.btn_save.removeClass("ui-icon-check")
        switch(state) {
            case "CLEAN":
                this.btn_save.css("background-color", '');
                this.btn_save.addClass("ui-icon-check")
                break;
            case "LOCAL":
                this.btn_save.css("background-color", 'orange');
                this.btn_save.addClass("ui-icon-check")
                break
            case "EXT":
                this.btn_save.css("background-color", 'green');
                this.btn_save.addClass("ui-icon-edit")
                break
            case "ERR":
                this.btn_save.css("background-color", 'orange');
                this.btn_save.addClass("ui-icon-alert")
                break
        }
    }

    show() {
        for (let window of this.windows) {
            window.show()
        }
    }

    hide = () => {
        for (let window of this.windows) {
            window.hide()
        }
    }

    delete = () => {
        if (! this.deleted) {
            this.socket.send(JSON.stringify({'cmd': {'cmd': 'delete', 'category': 'bus', 'name': this.name}}));
            this.hide()
            this.windows[0].show();
            this.deleted = true;
        }
    }

    save = () => {
        if (Object.keys(this.changed).includes("NAME")) {
            delete this.changed.NAME;
        }
        if ( Object.keys(this.changed).length ) {
            let msg = {'bus': {}}
            msg['bus'][this.name] = this.changed;
            this.socket.send(JSON.stringify(msg));
            this.changed = {};
        } else if (this.save_ready) {
            this.socket.send(JSON.stringify({'cmd': {'cmd': 'save'}}));
            if (this.deleted) {
                window.location.hash = "";
                location.reload()
            }
            return
        }
        if (this.name !== this.in_name.val()) {
            let new_name = check_new_name(this.in_name.val());
            let req = {'cmd': {'cmd': 'rename', 'category': 'bus'}};
            req['cmd']['name'] = this.name;
            req['cmd']['new-name'] = new_name;
            this.socket.send(JSON.stringify(req));
            window.location.hash = "#bus#" + new_name;
            location.reload()
        }
    }

    create_html() {
        let window_head = HtmlBuilder.add_window("dashboard", this.name + '-window-head');
        let btn_delete = HtmlBuilder.create_button("delete", this.delete);
        let btn_save = HtmlBuilder.create_button("check", this.save);
        btn_save.id = this.name + "-btn-save";
        let title_head = HtmlBuilder.create_input_with_buttons(this.name, btn_delete, btn_save, this.name+"-name");
        window_head.append(title_head);
        this.windows.push($('#' + this.name + '-window-head'));

        // ------------------------------------------

        let window_serial = HtmlBuilder.add_window("dashboard", this.name + '-window-serial');
        let title_serial= HtmlBuilder.create_title("Serial configuration", null);
        window_serial.append(title_serial);
        HtmlBuilder.add_string_input(window_serial, "Path", this.name + '-serial-path')

        HtmlBuilder.add_selector(window_serial, "Baud-rate",this.name + "-serial-baud", {
            2400: "2400",
            4800: "4800",
            9600: "9600",
            19200: "19200",
            28800: "28800",
            38400: "38400",
            57600: "57600",
        })

        HtmlBuilder.add_selector(window_serial, "Parity",this.name + "-serial-parity", {
            'N': "NONE",
            'E': "EVEN",
            'O': "ODD",
        })

        this.windows.push($('#' + this.name + '-window-serial'));

        // ------------------------------------------

        this.__initialize()
    }
}
