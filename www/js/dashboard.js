import { LedSCDashboard } from './ledSC.mjs';
import { port_ctrl } from "./conf.mjs"

let host = $(location).attr("hostname");
let triggers_list = $("#triggers-list");
let socket_ctrl = new WebSocket("ws://" + host + ":"+port_ctrl);
let devices = [];
let triggers = [];

socket_ctrl.onclose = (data) => {
    if (data.wasClean) {
        console.log("WS Connection closed");
    } else {
        console.log("WS Connection broken");
    }
    //console.log('code: ' + data.code + ' reason: ' + data.reason);
};

socket_ctrl.onerror = (error) => {
    console.log("WS Connection error")
}

socket_ctrl.onopen = (data) => {
    console.log("WS Connection opened");
};

socket_ctrl.onmessage = (message) => {
    if(message.data.length === 0) return;
    let data = JSON.parse(message.data);
    if ('dev' in data) {
        for (const [key, value] of Object.entries(data['dev'])) {
            // console.log(key, value);
            if (key in devices) {
                devices[key].set(value);
            } else {
                let device = new LedSCDashboard(key, socket_ctrl);
                device.create_html()
                device.set(value)
                devices[key] = device;
            }
        }
    }
    if ('trigger' in data) {
        for (const key of data['trigger']) {
            if (!triggers.includes(key)) {
                let item_label = document.createElement("a");
                item_label.setAttribute("class", "ui-btn ui-btn-icon-right ui-icon-carat-r")
                item_label.id = "trigger-" + key
                item_label.textContent = key;
                let item = document.createElement("li");
                item.append(item_label);
                triggers_list.append(item);
                triggers.push(key);
                $("#trigger-" + key).on("click", function () {
                    socket_ctrl.send(JSON.stringify({"trigger": [key]}));
                })
            }
        }
    }
}