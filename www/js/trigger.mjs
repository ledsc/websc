import {HtmlBuilder} from "./html_builder.mjs";

function check_new_name(new_name) {
    new_name = new_name.replace(/ /g, '-');
    new_name = new_name.replace(/:/g, '-');
    return new_name
}

export class Trigger {
    constructor(name, socket, devices) {
        this.name = name;
        this.socket = socket;
        this.windows = [];
        this.changed = false;
        this.save_ready = false;
        this.update_devices(devices)
        this.consequences = [];
        this.deleted = false;
    }

    update_devices = (buses) => {
        this.devices= {}
        for ( const [name, bus] of Object.entries(buses)) {
            this.devices[name] = name;
        }
    }

    set_changed = (is_changed) => {
        this.save_ready = is_changed;
        if(this.save_ready){
            this.set_state("EXT");
        } else {
            this.set_state("CLEAN");
        }
    }

    __initialize() {
        this.btn_save = $("#" + this.name + "-btn-save");
        this.consequences_list = $("#" + this.name + "-consequences-list")
        this.trigger_device = $("#" + this.name + "-trigger-device")
        this.trigger_event = $("#" + this.name + "-trigger-event")
        this.in_name = $("#" + this.name + "-name");

        let self = this;
        this.trigger_device.on("change", function () {self.set_state("LOCAL")})
        this.trigger_event.on("change", function (){
            if ( this.value === "none" ) {
                self.trigger_device.val("none");
                self.trigger_device.prop("disabled", true);
            } else if (self.trigger_device.prop("disabled")) {
                if (self.trigger_device.val() == null) self.trigger_device.val(Object.keys(self.devices)[0]);
                self.trigger_device.prop("disabled", false);
            }
            self.set_state("LOCAL");
        }); this.trigger_event.val("none").trigger("change");
        this.in_name.on("change", function(){self.changed = true; self.set_state("LOCAL")})
    }

    set = (data) => {
        let changed = this.changed;

        if('consequences' in data){this.clean_consequences(); this.refresh_consequences(data['consequences']);}
        if('device' in data){this.trigger_device.val(data['device'])}
        if('event' in data){this.trigger_event.val(data['event']).trigger("change");}

        if (changed){
            this.set_state("ERR")
        } else if (this.save_ready) {
            this.set_state("EXT");
        } else {
            this.set_state("CLEAN");
        }
    }

    set_state = (state) => {
        this.btn_save.removeClass("ui-icon-edit")
        this.btn_save.removeClass("ui-icon-check")
        switch(state) {
            case "CLEAN":
                this.btn_save.css("background-color", '');
                this.btn_save.addClass("ui-icon-check")
                break;
            case "LOCAL":
                this.btn_save.css("background-color", 'orange');
                this.btn_save.addClass("ui-icon-check")
                this.changed = true;
                break
            case "EXT":
                this.btn_save.css("background-color", 'green');
                this.btn_save.addClass("ui-icon-edit")
                this.changed = false;
                break
            case "ERR":
                this.btn_save.css("background-color", 'orange');
                this.btn_save.addClass("ui-icon-alert")
                break
        }
    }

    show() {
        for (let window of this.windows) {
            window.show()
        }
    }

    hide = () => {
        for (let window of this.windows) {
            window.hide()
        }
    }

    delete = () => {
        if (! this.deleted){
            this.socket.send(JSON.stringify({'cmd': {'cmd': 'delete', 'category': 'trigger', 'name': this.name}}));
            this.hide()
            this.windows[0].show();
            this.deleted = true;
        }
    }

    save = () => {
        if ( this.changed ) {
            let msg = {'trigger': {}}
            let consequences = []
            for ( let i = 0; i < this.consequences.length; i++) {
                name = this.name+"-con-" + i;
                let data = {"set": {}, "get": {}};
                data["set"]["dev"] = $("#" + name + "-setter-dev").val();
                data["set"]["var"] = $("#" + name + "-setter-var").val();
                data["get"]["dev"] = $("#" + name + "-getter-dev").val();
                data["get"]["var"] = $("#" + name + "-getter-var").val();
                if (data["get"]["dev"] === "<value>"){
                    data["get"]["var"] = $("#" + name + "-getter-value").val();
                }
                consequences.push(data);
            }
            msg['trigger'][this.name] = {
                'event': this.trigger_event.val(),
                'device': this.trigger_device.val(),
                'consequences': consequences
            };
            this.socket.send(JSON.stringify(msg));
            if (this.name !== this.in_name.val()) {
                let new_name = check_new_name(this.in_name.val());
                let req = {'cmd': {'cmd': 'rename', 'category': 'trigger'}};
                req['cmd']['name'] = this.name;
                req['cmd']['new-name'] = new_name;
                this.socket.send(JSON.stringify(req));
                window.location.hash = "#trigger#" + new_name;
                location.reload()
            }
            this.changed = false;
        } else if (this.save_ready) {
            this.socket.send(JSON.stringify({'cmd': {'cmd': 'save'}}));
            if (this.deleted) {
                window.location.hash = "";
                location.reload()
            }
        }
    }

    clean_consequences = () => {
        for ( let item of this.consequences) {
            $(item).remove();
        }
        this.consequences = [];
    }

    refresh_consequences = (consequences) => {
        let i = this.consequences.length;
        for (let consequence of consequences) {
            name = this.name+"-con-" + i;
            let container = document.createElement("div");
            container.style.setProperty("display", "flex");
            container.style.setProperty("align-items", "center");
            container.style.setProperty("justify-content", "space-around");

            let left = document.createElement("div");
            left.style.setProperty("display", "flex");
            left.style.setProperty("align-items", "center");
            left.style.setProperty("justify-content", "center");
            left.style.width = "100%";

            let right = document.createElement("div");
            right.style.setProperty("display", "flex");
            right.style.setProperty("align-items", "center");
            right.style.setProperty("justify-content", "center");
            right.style.width = "100%";


            HtmlBuilder.add_selector(left, null, name + "-setter-dev", this.devices, null, 90);
            HtmlBuilder.add_selector(left, " ", name + "-setter-var", {
                "trigger": "Power Trigger", "color": "Color", "TT": "Transition time",
                "R": "Red", "G": "Green", "B": "Blue", "W": "White"
            }, null, 10);

            let getters = {"<value>" :"Static value"};
            for (let [key, value] of Object.entries(this.devices)) {getters[key] = value;}
            HtmlBuilder.add_selector(right, null, name + "-getter-dev", getters, null, 90);
            HtmlBuilder.add_selector(right, " ", name + "-getter-var", {
                "color": "Color", "TT": "Transition time",
                "R": "Red", "G": "Green", "B": "Blue", "W": "White"
            }, null, 10);
            let _input = document.createElement("input")
            _input.setAttribute("class", "ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset")
            _input.id = name + "-getter-value";
            _input.type = "number";
            _input.style.height = "35px";
            _input.min = 0;
            _input.max = 65535;
            right.append(_input);

            container.append(left);
            let equals_title = document.createElement("h4");
            equals_title.textContent = " = ";
            container.append(equals_title);
            container.append(right);

            this.consequences_list.append(container);
            this.consequences.push(container);

            let setter_dev = $("#" + name + "-setter-dev")
            let setter_var = $("#" + name + "-setter-var");
            let getter_dev = $("#" + name + "-getter-dev");
            let getter_var = $("#" + name + "-getter-var");
            let getter_value = $("#" + name + "-getter-value")

            let _set = consequence['set'];
            let _get = consequence['get'];
            setter_dev.val(_set['dev']);
            setter_var.val(_set['var']);
            getter_dev.val(_get['dev']);
            getter_var.val(_get['var']);
            getter_value.val(_get['var']);

            this.check_consequence(i);

            let self = this;
            let ii = i;
            setter_dev.on("change", function (){self.set_state("LOCAL")});
            setter_var.on("change", function (){self.check_consequence(ii); self.set_state("LOCAL");});
            getter_dev.on("change", function (){self.check_consequence(ii); self.set_state("LOCAL");});
            getter_var.on("change", function (){self.check_consequence(ii); self.set_state("LOCAL");});
            getter_value.on("change", function (){self.set_state("LOCAL");});

            i += 1;
        }
    }

    check_consequence = (i) => {
        name = this.name+"-con-" + i;
        let in_var = $("#" + name + "-getter-var");
        let in_value = $("#" + name + "-getter-value");
        let in_dev = $("#" + name + "-getter-dev");
        let setter_var = $("#" + name + "-setter-var").val();

        if (setter_var === "trigger") {
            in_dev.val("<value>");
            in_value.val(1);
            in_dev.prop("disabled", true);
            in_value.prop("disabled", true);
            in_var.prop("disabled", true);
        } else if (setter_var === "color"){
            in_var.val("color");
            if (in_dev.val() === "<value>") {in_dev.val(null)}
            in_dev.prop("disabled", null);
            in_value.prop("disabled", true);
            in_var.prop("disabled", true);
        } else {
            if (in_var.val() === "color") {in_var.val(null)}
            in_dev.prop("disabled", null);
            in_value.prop("disabled", null);
            in_var.prop("disabled", null);
        }

        if (in_dev.val() === "<value>"){
            in_var.hide();
            in_value.show();
        } else {
            in_var.show();
            in_value.hide();
        }

    }

    add_consequence = () => {
        let get_dev = this.trigger_device.val();
        let keys = Object.keys(this.devices)
        if ( get_dev == null ) {
            get_dev = keys[keys.length - 1];
        }
        let set_dev = keys[Math.floor(Math.random() * keys.length)];
        this.refresh_consequences([{
            "set": {"dev": set_dev, "var": "color"},
            "get": {"dev": get_dev, "var": "color" }
            }]
        );
        this.set_state("LOCAL")
    }

    remove_consequence = () => {
        this.consequences.pop().remove();
        this.set_state("LOCAL")
    }

    create_html() {
        let window_head = HtmlBuilder.add_window("dashboard", this.name + '-window-head');
        let btn_delete = HtmlBuilder.create_button("delete", this.delete);
        let btn_save = HtmlBuilder.create_button("check", this.save);
        btn_save.id = this.name + "-btn-save";
        let title_head = HtmlBuilder.create_input_with_buttons(this.name, btn_delete, btn_save, this.name + "-name");
        window_head.append(title_head);
        this.windows.push($('#' + this.name + '-window-head'));

        // ------------------------------------------

        let window_trigger = HtmlBuilder.add_window("dashboard", this.name + '-window-trigger');
        let title_trigger = HtmlBuilder.create_title("Trigger event", null);
        window_trigger.append(title_trigger);
        HtmlBuilder.add_selector(window_trigger, "Event",this.name + "-trigger-event", {
            "none": "None",
            "px": "Triggered proximity",
            "color": "Changed color",
        })
        HtmlBuilder.add_selector(window_trigger, "Device", this.name+"-trigger-device", this.devices)
        this.windows.push($('#' + this.name + '-window-trigger'));

        // ------------------------------------------

        let window_consequence = HtmlBuilder.add_window("dashboard", this.name + '-window-consequence');
        let title_consequence= HtmlBuilder.create_title("Consequences", null);
        window_consequence.append(title_consequence);
        let container = HtmlBuilder.create_container(this.name + "-consequences-list");
        window_consequence.append(container);

        let div_manage_consequences = document.createElement("div")
        div_manage_consequences.style.setProperty("display", "flex");
        div_manage_consequences.style.setProperty("justify-content", "center");
        let btn_remove_consequence = HtmlBuilder.create_button("minus", this.remove_consequence);
        div_manage_consequences.append(btn_remove_consequence)
        let btn_add_consequence = HtmlBuilder.create_button("plus", this.add_consequence);
        div_manage_consequences.append(btn_add_consequence)
        window_consequence.append(div_manage_consequences);
        this.windows.push($('#' + this.name + '-window-consequence'));


        // ------------------------------------------

        this.__initialize()
    }
}

