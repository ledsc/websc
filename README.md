# WebSC

[LedSC] is a controller for LED strips that communicates via the Modbus protocol.
WebSC is a daemon that communicates with LedSC via Modbus and publishes its control and other advanced functions as a WebSocket API.


## Servers

You can set where individual APIs should be published. The 'host' and 'port' parameters are always needed for configuration.

### Control API

To set it up, use "server-ctrl" in the configuration.
This API is used for basic control of the LED strip.
You can control the brightness of individual colors and activate triggers 

### Configuration API

To set it up, use "server-conf" in the configuration.
This API is used to configure the daemon. 
Disclosure of this API is a security risk.
It can be used to change the configuration of the daemon.
It is therefore recommended to redirect this api to a safe place after setting the configuration,
or to disable it.
It can be used to configure:

- LedSC device 
    - Color channel distribution
    - Default color
    - Enable/Disable proximity sensor
    - ...
- RS485 channels 
    - System path to serial bus
    - Serial bus settings
- Triggers
    - Reactions to events can be set.
    - An event can be triggered based on a color change or proximity sensor activity.
    - Each event has its consequences, which can be configured.
- Servers
    - The configuration of the moderation of single APIs can be set using the API.

### Web application

To set it up, use "server-web" in the configuration.
The web application can use both websocket APIs.
We strongly recommend to use this application to configure daemon.
You can then control your LED strings using this application, or use our
[home-assistant integration](https://www.home-assistant.io/integrations/ledsc/).

## Configuration Example:

```yaml
log-level: DEBUG
server-ctrl:
  host: 0.0.0.0
  port: 8443
server-conf:
  host: 0.0.0.0
  port: 8444
server-web:
  host: 0.0.0.0
  port: 8080
virtual: true
buses:
  RS485_A:
    baud-rate: 19200
    parity: N
    path: /dev/ttyUSB0
    slaves:
      Kitchen:
        slave-id: 1
      Dinning-room:
        slave-id: 25
      Hall-end:
        slave-id: 12
  RS485_B:
    baud-rate: 19200
    parity: N
    path: /dev/ttyUSB1
    slaves:
      Hall-front:
        slave-id: 3
      Living-room:
        slave-id: 11
```

[LedSC]: https://ledsc.eu/
